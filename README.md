# tr2vrwrapper

Tomb Raider 2 virtual reality stereo rendering wrapper

- On Windows 10 enable DirectPlay (old DirectX) in Windows-Features under Control-Panel->Programs and Features->Enable/Disable Windows-Features->Legacy Components
- copy the ddraw.dll from the TR2VR-Project to the game directory with TOMB.exe. The TR2VR_project ddraw.dll will be used instead of the original one
- copy TR2VRSettings.txt to the game directory with TOMB.exe
- set the path in the TR2VRSettings.txt to the original windows ddraw.dll. TR2VR-Project ddraw.dll uses the original windows ddraw.dll
- set all other options if nessesary

