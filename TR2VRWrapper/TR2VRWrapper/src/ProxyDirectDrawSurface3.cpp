
#include "main.h"


ProxyDirectDrawSurface3::ProxyDirectDrawSurface3(IDirectDrawSurface3* aOriginal, unsigned int CreatorHierarchyLevel) : BaseDirectDrawSurface3(aOriginal, CreatorHierarchyLevel)
{
#ifdef SHOW_PROXYCALLS
	OutputDebugString((GetTabFill() + "ProxyDirectDrawSurface3 Constructor").c_str());
#endif
	DistortionCalculated = FALSE;
}

ProxyDirectDrawSurface3::~ProxyDirectDrawSurface3()
{
#ifdef SHOW_PROXYCALLS
	OutputDebugString((GetTabFill() + "ProxyDirectDrawSurface3 Destructor").c_str());
#endif
}

HRESULT WINAPI ProxyDirectDrawSurface3::Flip(LPDIRECTDRAWSURFACE3 pDDSurface, DWORD dwFlags)
{
//#ifdef SHOW_PROXYCALLS
//	OutputDebugString((GetTabFill() + "ProxyDirectDrawSurface3 Flip").c_str());
//#endif
//
//  LARGE_INTEGER t_start, t_stop, t_frequency;
//  double t_elapsed;
//
//  QueryPerformanceFrequency(&t_frequency);
//  
//
//  DDSURFACEDESC SourceSurfaceDesc;
//  DDSURFACEDESC DestSurfaceDesc;
//  DDSURFACEDESC RenderTargetDesc;
//
//  BaseDirectDrawSurface3* pSourceSurface = NULL;
//  BaseDirectDrawSurface3* pDestSurface = NULL;
//
//  BaseDirectDrawSurface3* pRenderTarget = NULL;
//  BaseDirectDrawSurface3* pBackBuffer = NULL;
//  BaseDirectDraw2* pDirectDraw2 = NULL;
//
//  DDSCAPS BackBufferCaps;
//  BackBufferCaps.dwCaps = DDSCAPS_BACKBUFFER;//DDSCAPS_3DDEVICE;
//
//  HRESULT hr = BaseDirectDrawSurface3::GetDDInterface((LPVOID*)(&pDirectDraw2));
//  if (FAILED(hr))
//	  return BaseDirectDrawSurface3::Flip(pDDSurface, dwFlags);
//
//  hr = BaseDirectDrawSurface3::GetAttachedSurface(&BackBufferCaps, (LPDIRECTDRAWSURFACE3*)&pBackBuffer);
//  if (FAILED(hr))
//	  return BaseDirectDrawSurface3::Flip(pDDSurface, dwFlags);
//  
//  
//  
//  pRenderTarget = (BaseDirectDrawSurface3*) pDirectDraw2->GetRenderTargetPointer();
//  
//
//  RenderTargetDesc.dwSize = sizeof(DDSURFACEDESC);
//  hr = pRenderTarget->GetSurfaceDesc(&RenderTargetDesc);
//  if (FAILED(hr))
//	  return BaseDirectDrawSurface3::Flip(pDDSurface, dwFlags);
//
//
//  SourceSurfaceDesc.dwSize = sizeof(DDSURFACEDESC);
//  SourceSurfaceDesc.dwFlags = DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH;
//  SourceSurfaceDesc.ddsCaps.dwCaps = DDSCAPS_OFFSCREENPLAIN | DDSCAPS_SYSTEMMEMORY;
//  SourceSurfaceDesc.dwWidth = RenderTargetDesc.dwWidth;
//  SourceSurfaceDesc.dwHeight = RenderTargetDesc.dwHeight;
//
//  DestSurfaceDesc = SourceSurfaceDesc;
//
//  pDirectDraw2->CreateSurface(&SourceSurfaceDesc, (LPDIRECTDRAWSURFACE*)&pSourceSurface, NULL);
//  pDirectDraw2->CreateSurface(&DestSurfaceDesc, (LPDIRECTDRAWSURFACE*)&pDestSurface, NULL);
//
//  
//  
//  QueryPerformanceCounter(&t_start);
//
//  //pSourceSurface->BltFast(0, 0, pRenderTarget, NULL, FALSE);
//  pSourceSurface->BltFast(0, 0, pBackBuffer, NULL, FALSE);
//
//  QueryPerformanceCounter(&t_stop);
//
//
//
//  pSourceSurface->Lock(NULL, &SourceSurfaceDesc, FALSE, NULL);
//  pDestSurface->Lock(NULL, &DestSurfaceDesc, FALSE, NULL);
//
//  D3DCOLOR* pSourceSurfaceMemory = (D3DCOLOR*)SourceSurfaceDesc.lpSurface;
//  D3DCOLOR* pDestSurfaceMemory = (D3DCOLOR*)DestSurfaceDesc.lpSurface;
//
//  if (!DistortionCalculated) {
//	ZeroMemory(DistortedCoord, sizeof(int)*MAX_DISTARRAYSIZE);
//	DistortionCalc(SourceSurfaceDesc.dwWidth, SourceSurfaceDesc.dwHeight);
//	DistortionCalculated = TRUE;
//  }
//
//  unsigned int SurfaceMemoryMaxCell = SourceSurfaceDesc.dwWidth*SourceSurfaceDesc.dwHeight - 1;
//
//  
//
//  for (unsigned int i = 0; i < SurfaceMemoryMaxCell; i+= 1) {
//	 // pDestSurfaceMemory[i] = (((pSourceSurfaceMemory[DistortedCoord[i]]&0x00FF0000)>>1)&0x00FF0000) + (((pSourceSurfaceMemory[DistortedCoord[i]]&0x0000FF00)>>1)&0x0000FF00) + (((pSourceSurfaceMemory[DistortedCoord[i]]&0x000000FF)>>1)&0x000000FF) +
//	//	  (((pSourceSurfaceMemory[DistortedCoord[i+1]]&0x00FF0000)>>1)&0x00FF0000) + (((pSourceSurfaceMemory[DistortedCoord[i+1]]&0x0000FF00)>>1)&0x0000FF00) + (((pSourceSurfaceMemory[DistortedCoord[i+1]]&0x000000FF)>>1)&0x000000FF);
//	 // pDestSurfaceMemory[i+1] = (((pSourceSurfaceMemory[DistortedCoord[i+1]]&0x00FF0000)>>1)&0x00FF0000) + (((pSourceSurfaceMemory[DistortedCoord[i+1]]&0x0000FF00)>>1)&0x0000FF00) + (((pSourceSurfaceMemory[DistortedCoord[i+1]]&0x000000FF)>>1)&0x000000FF) +
//	//	  (((pSourceSurfaceMemory[DistortedCoord[i+2]]&0x00FF0000)>>1)&0x00FF0000) + (((pSourceSurfaceMemory[DistortedCoord[i+2]]&0x0000FF00)>>1)&0x0000FF00) + (((pSourceSurfaceMemory[DistortedCoord[i+2]]&0x000000FF)>>1)&0x000000FF);
//
//	  pDestSurfaceMemory[i] = pSourceSurfaceMemory[DistortedCoord[i]];
//	  //pDestSurfaceMemory[i+1] = pSourceSurfaceMemory[DistortedCoord[i+1]];
//	  //pDestSurfaceMemory[i+2] = pSourceSurfaceMemory[DistortedCoord[i+2]];
//	  //pDestSurfaceMemory[i+3] = pSourceSurfaceMemory[DistortedCoord[i+3]];
//	  //pDestSurfaceMemory[i+4] = pSourceSurfaceMemory[DistortedCoord[i+4]];
//	  //pDestSurfaceMemory[i+5] = pSourceSurfaceMemory[DistortedCoord[i+5]];
//	  //pDestSurfaceMemory[i+6] = pSourceSurfaceMemory[DistortedCoord[i+6]];
//	  //pDestSurfaceMemory[i+7] = pSourceSurfaceMemory[DistortedCoord[i+7]];
//	  //pDestSurfaceMemory[i+8] = pSourceSurfaceMemory[DistortedCoord[i+8]];
//	  //pDestSurfaceMemory[i+9] = pSourceSurfaceMemory[DistortedCoord[i+9]];
//	  //if (i == 234348)
//		//  pDestSurfaceMemory[i] = 0x00FF0000;
//  }
//
//  
//
//
//  pSourceSurface->Unlock(NULL);
//  pDestSurface->Unlock(NULL);
//
//
//  pBackBuffer->BltFast(0, 0, pDestSurface, NULL, FALSE);
// 
//  pSourceSurface->Release();
//  pDestSurface->Release();
//  //pRenderTarget->Release();
//  pBackBuffer->Release();
//  
//  
//  
//  t_elapsed = ((t_start.QuadPart - t_stop.QuadPart) * 1000.0f / t_frequency.QuadPart);
//  char buff[64];
//  sprintf_s(buff, "Init: %f", t_elapsed);
//  OutputDebugString(buff);
//  
//  //dwFlags = DDFLIP_NOVSYNC;
  return BaseDirectDrawSurface3::Flip(pDDSurface, dwFlags);
}

HRESULT WINAPI ProxyDirectDrawSurface3::AddAttachedSurface(LPDIRECTDRAWSURFACE3 a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((GetTabFill()  + "ProxyDirectDrawSurface3 AddAttachedSurface").c_str());
#endif

  //The RefCount of the surface being attached is incremented. Since my version of Tomb Raider 2 doesn't use DeleteAttachedSurface 
  //and the surface is released only once, the RefCount of the original non-redirected attached surface (which was created by the game)
  //doesn't decrement to zero (it stays one at exit from DirectDraw).
  //Adding a RefCount to the BaseDirectDrawSurface3 would then result in a memory leak in the BaseDirectDrawSurface3 class. So one must release the original and the wrapper.

  HRESULT hr = BaseDirectDrawSurface3::AddAttachedSurface(a);
  ((BaseDirectDrawSurface3 *)a)->GetOriginal()->Release();
  ((BaseDirectDrawSurface3 *)a)->Release();
  return hr;
}


//void ProxyDirectDrawSurface3::DistortionCalc(int SurfaceWidth, int SurfaceHeight)
//{
//  float xSourcePos;
//  float ySourcePos;
//  float xScaled;
//  float yScaled;
//  float rSqrDestPos;
//  float distortionscale;
//  float OutputScale = 0.8f;
// // float LensCenterX = (1.0f - 2*64/149.76f);
//  float LensCenterX = 4.0f*(float)LENSOFFSETFROMSCREENCENTER_MM/(float)SCREENSIZE_MM;
//
//  	  int leftx = 0;
//	  int lefty = 0;
//	  int rightx = 0;
//	  int righty = 0;
//	  int topx = 0;
//	  int topy = 0;
//	  int midx = 0;
//	  int midy = 0;
//	  int bottomx = 0;
//	  int bottomy = 0;
//	  int midxright = 0;
//	  int midyright = 0;
//  for (int yDestPos = -SurfaceHeight/2; yDestPos < SurfaceHeight/2; yDestPos++)
//  {
//	  //for (int xDestPos = -SurfaceWidth/4; xDestPos < SurfaceWidth/4; xDestPos++)
//	  //{
//		 // xScaled = (float)xDestPos/(SurfaceWidth/4) - LensCenterX;
//		 // yScaled = (float)yDestPos/(SurfaceHeight/2);
//		 // rSqrDestPos = xScaled*xScaled + yScaled*yScaled;
//		 // distortionscale = OutputScale*(1 + 0.22f*rSqrDestPos + 0.24f*rSqrDestPos*rSqrDestPos);
//		 // xSourcePos = SurfaceWidth/4 + (SurfaceWidth/4)*(LensCenterX + xScaled*distortionscale);
//		 // ySourcePos = SurfaceHeight/2 + (SurfaceHeight/2)*yScaled*distortionscale;
//		 // if ((xSourcePos < 0 ) || (xSourcePos > 959 ) || (ySourcePos < 0 ) || (ySourcePos > 1079 ))
//			//  DistortedCoord[xDestPos + SurfaceWidth/4 + (yDestPos + SurfaceHeight/2)*SurfaceWidth] = 10;
//		 // else
//			//DistortedCoord[xDestPos + SurfaceWidth/4 + (yDestPos + SurfaceHeight/2)*SurfaceWidth] = (int)floor(xSourcePos + 0.5f) + (int)(floor(ySourcePos + 0.5f))*SurfaceWidth;	  
//	  //}
//
//	  for (int xDestPos = -SurfaceWidth/4; xDestPos < SurfaceWidth/4; xDestPos++) {
//		  DistortedCoord[xDestPos + SurfaceWidth/4 + (yDestPos + SurfaceHeight/2)*SurfaceWidth] = xDestPos + SurfaceWidth/4 + (yDestPos + SurfaceHeight/2)*SurfaceWidth;
//	  }
//
//
//	  for (int xDestPos = -SurfaceWidth/4; xDestPos < SurfaceWidth/4; xDestPos++)
//	  {
//		  xScaled = (float)xDestPos/(SurfaceWidth/4) + LensCenterX;
//		  yScaled = (float)yDestPos/(SurfaceHeight/2);
//		  rSqrDestPos = xScaled*xScaled + yScaled*yScaled;
//		  distortionscale = OutputScale*(1 + 0.22f*rSqrDestPos + 0.24f*rSqrDestPos*rSqrDestPos);
//		  xSourcePos = 3*SurfaceWidth/4 + (SurfaceWidth/4)*(xScaled*distortionscale - LensCenterX);
//		  ySourcePos = SurfaceHeight/2 + (SurfaceHeight/2)*yScaled*distortionscale;
//		  if ((xSourcePos < 960 ) || (xSourcePos > 1919 ) || (ySourcePos < 0 ) || (ySourcePos > 1079 ))
//			  DistortedCoord[xDestPos + 3*SurfaceWidth/4 + (yDestPos + SurfaceHeight/2)*SurfaceWidth] = 10;
//		  else {
//
//			if (floor(ySourcePos + 0.5f) < 3) {
//				//char buff[64];
//				//sprintf_s(buff, "x:%f", xSourcePos);
//				//OutputDebugString(buff);
//
//				if ((floor(xSourcePos + 0.5f) == 960)||(floor(xSourcePos + 0.5f) == 961)) {
//					leftx = xDestPos;
//					lefty = yDestPos;
//				}
//				if (floor(xSourcePos + 0.5f) == 1440) {
//					topx = xDestPos;
//					topy = yDestPos;
//				}
//				if ((floor(xSourcePos + 0.5f) == 1919)||(floor(xSourcePos + 0.5f) == 1918)) {
//					rightx = xDestPos;
//					righty = yDestPos;
//				}
//			}
//
//			if (floor(xSourcePos + 0.5f) < 963) {
//				if ((floor(ySourcePos + 0.5f) == 540)||(floor(ySourcePos + 0.5f) == 541)) {
//					midx = xDestPos;
//					midy = yDestPos;
//				}
//
//				if ((floor(ySourcePos + 0.5f) == 1079)||(floor(ySourcePos + 0.5f) == 1077)) {
//					bottomx = xDestPos;
//					bottomy = yDestPos;
//				}
//			}
//
//			if (floor(xSourcePos + 0.5f) > 1916) {
//				if ((floor(ySourcePos + 0.5f) == 540)||(floor(ySourcePos + 0.5f) == 541)) {
//					midxright = xDestPos;
//					midyright = yDestPos;
//				}
//
//			}
//			DistortedCoord[xDestPos + 3*SurfaceWidth/4 + (yDestPos + SurfaceHeight/2)*SurfaceWidth] = (int)floor(xSourcePos + 0.5f) + (int)(floor(ySourcePos + 0.5f))*SurfaceWidth;	  //
//		  }
//	  }
//	  	  for (int xDestPos = -SurfaceWidth/4; xDestPos < SurfaceWidth/4; xDestPos++) {
//		  DistortedCoord[xDestPos + 3*SurfaceWidth/4 + (yDestPos + SurfaceHeight/2)*SurfaceWidth] = xDestPos + 3*SurfaceWidth/4 + (yDestPos + SurfaceHeight/2)*SurfaceWidth;
//			}
//  }
//
//  DistortedCoord[leftx + 3*SurfaceWidth/4 + (lefty + SurfaceHeight/2)*SurfaceWidth] = 0;
//  DistortedCoord[topx + 3*SurfaceWidth/4 + (topy + SurfaceHeight/2)*SurfaceWidth] = 0;
//  DistortedCoord[rightx + 3*SurfaceWidth/4 + (righty + SurfaceHeight/2)*SurfaceWidth] = 0;
//  DistortedCoord[midx + 3*SurfaceWidth/4 + (midy + SurfaceHeight/2)*SurfaceWidth] = 0;
//  DistortedCoord[bottomx + 3*SurfaceWidth/4 + (bottomy + SurfaceHeight/2)*SurfaceWidth] = 0;
//  DistortedCoord[midxright + 3*SurfaceWidth/4 + (midyright + SurfaceHeight/2)*SurfaceWidth] = 0;
//
//  DistortedCoord[-leftx + SurfaceWidth/4 + (lefty + SurfaceHeight/2)*SurfaceWidth] = 0;
//  DistortedCoord[-topx + SurfaceWidth/4 + (topy + SurfaceHeight/2)*SurfaceWidth] = 0;
//  DistortedCoord[-rightx + SurfaceWidth/4 + (righty + SurfaceHeight/2)*SurfaceWidth] = 0;
//  DistortedCoord[-midx + SurfaceWidth/4 + (midy + SurfaceHeight/2)*SurfaceWidth] = 0;
//  DistortedCoord[-bottomx + SurfaceWidth/4 + (bottomy + SurfaceHeight/2)*SurfaceWidth] = 0;
//  DistortedCoord[-midxright + SurfaceWidth/4 + (midyright + SurfaceHeight/2)*SurfaceWidth] = 0;
//
//	char buff[256];
//	sprintf_s(buff, "leftx:%d, lefty:%d, topx:%d, topy:%d, rightx:%d, righty:%d", leftx+SurfaceWidth/4, lefty+SurfaceHeight/2, topx+SurfaceWidth/4, topy+SurfaceHeight/2, rightx+SurfaceWidth/4, righty+SurfaceHeight/2);
//	OutputDebugString(buff);
//	sprintf_s(buff, "midx:%d, midy:%d, bottomx:%d, bottomy:%d", midx+SurfaceWidth/4, midy+SurfaceHeight/2, bottomx+SurfaceWidth/4, bottomy+SurfaceHeight/2);
//	OutputDebugString(buff);
//
//}

