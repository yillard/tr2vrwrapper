#include "main.h"

std::string RefIIDToName(REFIID riid)
{
#define SearchRefIIDName(x) \
	if(riid == IID_##x) \
	 return "IID_"###x;

	SearchRefIIDName(IDirect3D);
	SearchRefIIDName(IDirect3D2);
	SearchRefIIDName(IDirect3D3);
	SearchRefIIDName(IDirect3D7);
	SearchRefIIDName(IDirect3DDevice);
	SearchRefIIDName(IDirect3DDevice2);
	SearchRefIIDName(IDirect3DDevice3);
	SearchRefIIDName(IDirect3DDevice7);
	SearchRefIIDName(IDirect3DExecuteBuffer);
	SearchRefIIDName(IDirect3DLight);
	SearchRefIIDName(IDirect3DMaterial);
	SearchRefIIDName(IDirect3DMaterial2);
	SearchRefIIDName(IDirect3DMaterial3);
	SearchRefIIDName(IDirect3DTexture);
	SearchRefIIDName(IDirect3DTexture2);
	SearchRefIIDName(IDirect3DVertexBuffer);
	SearchRefIIDName(IDirect3DVertexBuffer7);
	SearchRefIIDName(IDirect3DViewport);
	SearchRefIIDName(IDirect3DViewport2);
	SearchRefIIDName(IDirect3DViewport3);
	SearchRefIIDName(IDirectDraw);
	SearchRefIIDName(IDirectDraw2);
	SearchRefIIDName(IDirectDraw4);
	SearchRefIIDName(IDirectDraw7);
	SearchRefIIDName(IDirectDrawClipper);
	SearchRefIIDName(IDirectDrawColorControl);
	SearchRefIIDName(IDirectDrawGammaControl);
	SearchRefIIDName(IDirectDrawPalette);
	SearchRefIIDName(IDirectDrawSurface);
	SearchRefIIDName(IDirectDrawSurface2);
	SearchRefIIDName(IDirectDrawSurface3);
	SearchRefIIDName(IDirectDrawSurface4);
	SearchRefIIDName(IDirectDrawSurface7);

	return "RefIIDName Not Found";
}
