#include "main.h"


BaseDirectDrawSurface3::BaseDirectDrawSurface3(IDirectDrawSurface3 * aOriginal, unsigned int CreatorHierarchyLevel) :
	mOriginal(aOriginal),
	m_nRefCount(1),
	m_nHierarchyLevel(CreatorHierarchyLevel + 1),
	m_strTabFill()

{
#ifdef SHOW_BASECALLS
	
	for (unsigned int i = 0; i < m_nHierarchyLevel; i++)
	{
		m_strTabFill = m_strTabFill + "    ";
	}

	char buff[64];
	sprintf_s(buff, "mOriginal:0x%x, wrapper:0x%x |  ", mOriginal, this);
	m_strTabFill = m_strTabFill + buff;
	OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 Constructor").c_str());
#endif
}

BaseDirectDrawSurface3::~BaseDirectDrawSurface3()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 Destructor").c_str());
	
#endif
	
	if(mOriginal) {
		ULONG OriginalRefCount = mOriginal->Release();
		//char buff[64];
		//sprintf_s(buff, "OriginalRefCount: %d", OriginalRefCount);
		//OutputDebugString(buff);
	}

}

HRESULT __stdcall BaseDirectDrawSurface3::QueryInterface(REFIID riid, LPVOID FAR * ppvObj)
{
#ifdef SHOW_BASECALLS
  	OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 QueryInterface").c_str());
#endif
	HRESULT hr = mOriginal->QueryInterface(riid, ppvObj);
	if (FAILED(hr))
		return hr;

#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill  + "Found " + RefIIDToName(riid)).c_str());
#endif

	if (riid == IID_IDirect3DTexture2)
	{
#ifdef SHOW_BASECALLS
		OutputDebugString((m_strTabFill + "Create BaseDirect3DTexture2").c_str());
#endif

		auto pValuePair = original_to_wrapper_map.find(static_cast<LPDIRECT3DTEXTURE2>(*ppvObj));
		if (pValuePair != original_to_wrapper_map.end())
		{
			*ppvObj = std::get<std::unique_ptr<BaseDirect3DTexture2>>(pValuePair->second).get();
			return hr;
		}

		original_to_wrapper_map.emplace(std::make_pair(static_cast<LPDIRECT3DTEXTURE2>(*ppvObj), std::make_unique<BaseDirect3DTexture2>(static_cast<LPDIRECT3DTEXTURE2>(*ppvObj), m_nHierarchyLevel)));
		*ppvObj = std::get<std::unique_ptr<BaseDirect3DTexture2>>(original_to_wrapper_map.at(static_cast<LPDIRECT3DTEXTURE2>(*ppvObj))).get();
	}
		
	return hr;
}

ULONG __stdcall BaseDirectDrawSurface3::AddRef()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 AddRef").c_str());
#endif
	return ++m_nRefCount;
}

ULONG __stdcall BaseDirectDrawSurface3::Release()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 Release").c_str());
#endif

	m_nRefCount--;

#ifdef SHOW_BASECALLS
	char buff[64];
	sprintf_s(buff, "WrapperRefCount:%d, 0x%x", m_nRefCount, this);
	OutputDebugString((m_strTabFill + buff).c_str());
#endif

	if (m_nRefCount == 0)
	{
		original_to_wrapper_map.erase(mOriginal);
		return 0;
	}
	return m_nRefCount;
}

HRESULT __stdcall BaseDirectDrawSurface3::AddAttachedSurface(LPDIRECTDRAWSURFACE3 a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 AddAttachedSurface").c_str());
#endif
  
  HRESULT hr = mOriginal->AddAttachedSurface((a)?((BaseDirectDrawSurface3 *)a)->GetOriginal():0);
  if(FAILED(hr))
	  return hr;
  ((BaseDirectDrawSurface3*)a)->AddRef();
  return hr;
}

HRESULT __stdcall BaseDirectDrawSurface3::AddOverlayDirtyRect(LPRECT a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 AddOverlayDirtyRect").c_str());
#endif
  return mOriginal->AddOverlayDirtyRect(a);
}

HRESULT __stdcall BaseDirectDrawSurface3::Blt(LPRECT a, LPDIRECTDRAWSURFACE3 b, LPRECT c, DWORD d, LPDDBLTFX e)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 Blt").c_str());
#endif
  return mOriginal->Blt(a, (b)?((BaseDirectDrawSurface3 *)b)->GetOriginal():0, c, d, e);
}

HRESULT __stdcall BaseDirectDrawSurface3::BltBatch(LPDDBLTBATCH a, DWORD b, DWORD c)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 BltBatch").c_str());
#endif
  return mOriginal->BltBatch(a, b, c);
}

HRESULT __stdcall BaseDirectDrawSurface3::BltFast(DWORD a, DWORD b, LPDIRECTDRAWSURFACE3 c, LPRECT d, DWORD e)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 BltFast").c_str());
#endif
  return mOriginal->BltFast(a, b, ((BaseDirectDrawSurface3 *)c)->GetOriginal(), d, e);
}

HRESULT __stdcall BaseDirectDrawSurface3::DeleteAttachedSurface(DWORD a, LPDIRECTDRAWSURFACE3 b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 DeleteAttachedSurface").c_str());
#endif
  HRESULT hr = mOriginal->DeleteAttachedSurface(a, (b)?((BaseDirectDrawSurface3 *)b)->GetOriginal():0);
  if (FAILED(hr))
	  return hr;
  if(b)
	  ((BaseDirectDrawSurface3*)b)->Release();
  else 
	  OutputDebugString("DeleteAttachedSurface -> MemoryLeak"); //ToDo:Keep track of attached surfaces for the case of deattaching them all at once (b=NULL)
  return hr;
}

HRESULT __stdcall BaseDirectDrawSurface3::EnumAttachedSurfaces(LPVOID a, LPDDENUMSURFACESCALLBACK b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 EnumAttachedSurfaces").c_str());
#endif
  return mOriginal->EnumAttachedSurfaces(a, b);
}

HRESULT __stdcall BaseDirectDrawSurface3::EnumOverlayZOrders(DWORD a, LPVOID b, LPDDENUMSURFACESCALLBACK c)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 EnumOverlayZOrders").c_str());
#endif
  return mOriginal->EnumOverlayZOrders(a, b, c);
}

HRESULT __stdcall BaseDirectDrawSurface3::Flip(LPDIRECTDRAWSURFACE3 a, DWORD b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 Flip").c_str());
#endif
  return mOriginal->Flip((a)?((BaseDirectDrawSurface3 *)a)->GetOriginal():0, b);
}

HRESULT __stdcall BaseDirectDrawSurface3::GetAttachedSurface(LPDDSCAPS a, LPDIRECTDRAWSURFACE3 FAR * b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 GetAttachedSurface").c_str());
#endif
  HRESULT hr = mOriginal->GetAttachedSurface(a, b);
  if (FAILED(hr))
	  return hr;

  auto pValuePair = original_to_wrapper_map.find(*b);
  if (pValuePair != original_to_wrapper_map.end())
  {
	  (*b)->Release();
	  *b = std::get<std::unique_ptr<BaseDirectDrawSurface3>>(pValuePair->second).get(); //Proxy crashes -> Proxy not needed
	  (*b)->AddRef();
	  return hr;
  }

  original_to_wrapper_map.emplace(std::make_pair(*b, std::make_unique<BaseDirectDrawSurface3>(*b, m_nHierarchyLevel))); //Proxy crashes -> Proxy not needed
  *b = std::get<std::unique_ptr<BaseDirectDrawSurface3>>(original_to_wrapper_map.at(*b)).get(); //Proxy crashes -> Proxy not needed

  return hr;
}

HRESULT __stdcall BaseDirectDrawSurface3::GetBltStatus(DWORD a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 GetBltStatus").c_str());
#endif
  return mOriginal->GetBltStatus(a);
}

HRESULT __stdcall BaseDirectDrawSurface3::GetCaps(LPDDSCAPS a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 GetCaps").c_str());
#endif
  return mOriginal->GetCaps(a);
}

HRESULT __stdcall BaseDirectDrawSurface3::GetClipper(LPDIRECTDRAWCLIPPER FAR * a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 GetClipper").c_str());
#endif
  return mOriginal->GetClipper(a);
}

HRESULT __stdcall BaseDirectDrawSurface3::GetColorKey(DWORD a, LPDDCOLORKEY b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 GetColorKey").c_str());
#endif
  return mOriginal->GetColorKey(a, b);
}

HRESULT __stdcall BaseDirectDrawSurface3::GetDC(HDC FAR * a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 GetDC").c_str());
#endif
  return mOriginal->GetDC(a);
}

HRESULT __stdcall BaseDirectDrawSurface3::GetFlipStatus(DWORD a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 GetFlipStatus").c_str());
#endif
  return mOriginal->GetFlipStatus(a);
}

HRESULT __stdcall BaseDirectDrawSurface3::GetOverlayPosition(LPLONG a, LPLONG b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 GetOverlayPosition").c_str());
#endif
  return mOriginal->GetOverlayPosition(a, b);
}

HRESULT __stdcall BaseDirectDrawSurface3::GetPalette(LPDIRECTDRAWPALETTE FAR * a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 GetPalette").c_str());
#endif
  return mOriginal->GetPalette(a);
}

HRESULT __stdcall BaseDirectDrawSurface3::GetPixelFormat(LPDDPIXELFORMAT a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 GetPixelFormat").c_str());
#endif
  return mOriginal->GetPixelFormat(a);
}

HRESULT __stdcall BaseDirectDrawSurface3::GetSurfaceDesc(LPDDSURFACEDESC a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 GetSurfaceDesc").c_str());
#endif
  return mOriginal->GetSurfaceDesc(a);
}

HRESULT __stdcall BaseDirectDrawSurface3::Initialize(LPDIRECTDRAW a, LPDDSURFACEDESC b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 Initialize").c_str());
#endif
  return DDERR_ALREADYINITIALIZED;
}

HRESULT __stdcall BaseDirectDrawSurface3::IsLost()
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 IsLost").c_str());
#endif
  return mOriginal->IsLost();
}

HRESULT __stdcall BaseDirectDrawSurface3::Lock(LPRECT a, LPDDSURFACEDESC b, DWORD c, HANDLE d)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 Lock").c_str());
#endif
  return mOriginal->Lock(a, b, c, d);
}

HRESULT __stdcall BaseDirectDrawSurface3::ReleaseDC(HDC a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 ReleaseDC").c_str());
#endif
  return mOriginal->ReleaseDC(a);
}

HRESULT __stdcall BaseDirectDrawSurface3::Restore()
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 Restore").c_str());
#endif
  return mOriginal->Restore();
}

HRESULT __stdcall BaseDirectDrawSurface3::SetClipper(LPDIRECTDRAWCLIPPER a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 SetClipper").c_str());
#endif
  //HRESULT x = mOriginal->SetClipper((a)?((myIDirectDrawClipper *)a)->mOriginal:0);
  return mOriginal->SetClipper(a);
}

HRESULT __stdcall BaseDirectDrawSurface3::SetColorKey(DWORD a, LPDDCOLORKEY b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 SetColorKey").c_str());
#endif
  return mOriginal->SetColorKey(a, b);
}

HRESULT __stdcall BaseDirectDrawSurface3::SetOverlayPosition(LONG a, LONG b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 SetOverlayPosition").c_str());
#endif
  return mOriginal->SetOverlayPosition(a, b);
}

HRESULT __stdcall BaseDirectDrawSurface3::SetPalette(LPDIRECTDRAWPALETTE a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 SetPalette").c_str());
#endif
  //HRESULT x = mOriginal->SetPalette((a)?((myIDirectDrawPalette *)a)->mOriginal:0);
  return mOriginal->SetPalette(a);
}

HRESULT __stdcall BaseDirectDrawSurface3::Unlock(LPVOID a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 Unlock").c_str());
#endif
  return mOriginal->Unlock(a);
}

HRESULT __stdcall BaseDirectDrawSurface3::UpdateOverlay(LPRECT a, LPDIRECTDRAWSURFACE3 b, LPRECT c, DWORD d, LPDDOVERLAYFX e)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 UpdateOverlay").c_str());
#endif
  return mOriginal->UpdateOverlay(a, (b)?((BaseDirectDrawSurface3 *)b)->GetOriginal():0, c, d, e);
}

HRESULT __stdcall BaseDirectDrawSurface3::UpdateOverlayDisplay(DWORD a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 UpdateOverlayDisplay").c_str());
#endif
  return mOriginal->UpdateOverlayDisplay(a);
}

HRESULT __stdcall BaseDirectDrawSurface3::UpdateOverlayZOrder(DWORD a, LPDIRECTDRAWSURFACE3 b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 UpdateOverlayZOrder").c_str());
#endif
  return mOriginal->UpdateOverlayZOrder(a, (b)?((BaseDirectDrawSurface3 *)b)->GetOriginal():0);
}

HRESULT __stdcall BaseDirectDrawSurface3::GetDDInterface(LPVOID FAR * a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 GetDDInterface").c_str());
#endif
  HRESULT hr = mOriginal->GetDDInterface(a);

  auto pValuePair = original_to_wrapper_map.find(static_cast<LPDIRECTDRAWSURFACE3>(*a));
  if (pValuePair != original_to_wrapper_map.end())
  {
	  *a = std::get<std::unique_ptr<BaseDirectDrawSurface3>>(pValuePair->second).get();
	  return hr;
  }
  
  return DDERR_INVALIDOBJECT;
}

HRESULT __stdcall BaseDirectDrawSurface3::PageLock(DWORD a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 PageLock").c_str());
#endif
  return mOriginal->PageLock(a);
}

HRESULT __stdcall BaseDirectDrawSurface3::PageUnlock(DWORD a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 PageUnlock").c_str());
#endif
  return mOriginal->PageUnlock(a);
}

HRESULT __stdcall BaseDirectDrawSurface3::SetSurfaceDesc(LPDDSURFACEDESC a, DWORD b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface3 SetSurfaceDesc").c_str());
#endif
  return mOriginal->SetSurfaceDesc(a, b);
}

IDirectDrawSurface3* BaseDirectDrawSurface3::GetOriginal()
{
	return mOriginal;
}

std::string BaseDirectDrawSurface3::GetTabFill()
{
	return m_strTabFill;
}