#include "main.h"


BaseDirect3D2::BaseDirect3D2(IDirect3D2 * aOriginal, unsigned int CreatorHierarchyLevel, BaseDirectDraw2* pCreatedBy) :
	mOriginal(aOriginal),
	mCreatedBy(pCreatedBy),
	m_nRefCount(1),
	m_nHierarchyLevel(CreatorHierarchyLevel +1),
	m_strTabFill()

{
#ifdef SHOW_BASECALLS
	//auto m_strTabFill = std::string {};
	for (unsigned int i = 0; i < m_nHierarchyLevel; i++)
	{
		m_strTabFill = m_strTabFill + "    ";
	}

	char buff[64];
	sprintf_s(buff, "mOriginal:0x%x, wrapper:0x%x |  ", mOriginal, this);
	m_strTabFill = m_strTabFill + buff;
	OutputDebugString((m_strTabFill + "BaseDirect3D2 Constructor").c_str());
#endif
}

BaseDirect3D2::~BaseDirect3D2()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3D2 Destructor").c_str());
#endif
	
	if(mOriginal)
		mOriginal->Release();
}


HRESULT WINAPI BaseDirect3D2::QueryInterface(REFIID riid, LPVOID * ppvObj)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3D2 QueryInterface").c_str());
#endif
  	HRESULT hr = mOriginal->QueryInterface(riid, ppvObj);
	if (FAILED(hr))
		return hr;
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill  + "Found " + RefIIDToName(riid)).c_str());
#endif
	
	return hr;
}

ULONG WINAPI BaseDirect3D2::AddRef()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3D2 AddRef").c_str());
#endif
	return ++m_nRefCount;
}

ULONG WINAPI BaseDirect3D2::Release()
{
#ifdef SHOW_BASECALLS
 	OutputDebugString((m_strTabFill + "BaseDirect3D2 Release").c_str());
#endif

	m_nRefCount--;
#ifdef SHOW_BASECALLS
	char buff[64];
	sprintf_s(buff, "RefCount:%d, 0x%x", m_nRefCount, this);
	OutputDebugString((m_strTabFill + buff).c_str());
#endif

	if (m_nRefCount == 0)
	{
		original_to_wrapper_map.erase(mOriginal);
		return 0;
	}
	return m_nRefCount;
}

HRESULT WINAPI BaseDirect3D2::EnumDevices(LPD3DENUMDEVICESCALLBACK a, LPVOID b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirect3D2 EnumDevices").c_str());
#endif
	return mOriginal->EnumDevices(a, b);
}

HRESULT WINAPI BaseDirect3D2::CreateLight(LPDIRECT3DLIGHT * a, IUnknown * b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3D2 CreateLight").c_str());
#endif
	return mOriginal->CreateLight(a, b);
}

HRESULT WINAPI BaseDirect3D2::CreateMaterial(LPDIRECT3DMATERIAL2 * a, IUnknown * b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3D2 CreateMaterial").c_str());
#endif
	HRESULT hr = mOriginal->CreateMaterial(a, b);
	if(FAILED(hr))
		return hr;

	auto pValuePair = original_to_wrapper_map.find(*a);
	if (pValuePair != original_to_wrapper_map.end())
	{
		*a = std::get< std::unique_ptr<BaseDirect3DMaterial2>>(pValuePair->second).get();
		return hr;
	}

	original_to_wrapper_map.emplace(std::make_pair(*a, std::make_unique<BaseDirect3DMaterial2>(*a, m_nHierarchyLevel)));
	*a = std::get< std::unique_ptr<BaseDirect3DMaterial2>>(original_to_wrapper_map.at(*a)).get();

	return hr;
}

HRESULT WINAPI BaseDirect3D2::CreateViewport(LPDIRECT3DVIEWPORT2 * a, IUnknown * b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3D2 CreateViewport").c_str());
#endif
	return mOriginal->CreateViewport(a, b);
}

HRESULT WINAPI BaseDirect3D2::FindDevice(LPD3DFINDDEVICESEARCH a, LPD3DFINDDEVICERESULT b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3D2 FindDevice").c_str());
#endif
	return mOriginal->FindDevice(a, b);
}

HRESULT WINAPI BaseDirect3D2::CreateDevice(REFCLSID a, LPDIRECTDRAWSURFACE b, LPDIRECT3DDEVICE2 *  lplpD3DDevice)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3D2 CreateDevice").c_str());
#endif

	HRESULT hr = mOriginal->CreateDevice(a, (b)?((BaseDirectDrawSurface*)b)->GetOriginal():0, lplpD3DDevice);
	if (FAILED(hr))
		return hr;

#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "Create BaseDirect3DDevice2").c_str());
#endif

	auto pValuePair = original_to_wrapper_map.find(*lplpD3DDevice);
	if (pValuePair != original_to_wrapper_map.end())
	{
		*lplpD3DDevice = std::get< std::unique_ptr<ProxyDirect3DDevice2>>(pValuePair->second).get(); //Proxy
		return hr;
	}

	original_to_wrapper_map.emplace(std::make_pair(*lplpD3DDevice, std::make_unique<ProxyDirect3DDevice2>(*lplpD3DDevice, m_nHierarchyLevel, this))); //Proxy
	*lplpD3DDevice = std::get< std::unique_ptr<ProxyDirect3DDevice2>>(original_to_wrapper_map.at(*lplpD3DDevice)).get(); //Proxy

	return hr;
}

BaseDirectDraw2* BaseDirect3D2::GetDDInterface()
{
	return mCreatedBy;
}