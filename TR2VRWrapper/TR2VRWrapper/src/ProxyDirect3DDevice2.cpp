#include "main.h"


ProxyDirect3DDevice2::ProxyDirect3DDevice2(IDirect3DDevice2 * aOriginal, unsigned int CreatorHierarchyLevel, BaseDirect3D2* pCreatedBy) : BaseDirect3DDevice2(aOriginal, CreatorHierarchyLevel, pCreatedBy)
{
#ifdef SHOW_PROXYCALLS
	OutputDebugString((GetTabFill() + "ProxyDirect3DDevice2 Constructor").c_str());
#endif

	bSphereCreated = FALSE;

	pCurrentDirect3DViewport2 = NULL;
	pD3DInterface = NULL;
	ProxyDirect3DDevice2::GetDirect3D((LPDIRECT3D2*)&pD3DInterface);

	pNewLeftRenderTarget = NULL;
	pNewRightRenderTarget = NULL;
	pOldRenderTarget = NULL;
	pLeftZBuffer=NULL;
	pRightZBuffer=NULL;
	DDSURFACEDESC NewRenderTargetDesc;
	DDSURFACEDESC OldRenderTargetDesc;
	
	ZeroMemory(&NewRenderTargetDesc, sizeof(DDSURFACEDESC));
	ZeroMemory(&OldRenderTargetDesc, sizeof(DDSURFACEDESC));
	
	BaseDirect3DDevice2::GetRenderTarget((LPDIRECTDRAWSURFACE*)&pOldRenderTarget);
	
	OldRenderTargetDesc.dwSize = sizeof(DDSURFACEDESC);
	pOldRenderTarget->GetSurfaceDesc(&OldRenderTargetDesc);

	dwWidth = OldRenderTargetDesc.dwWidth;
	dwHeight = OldRenderTargetDesc.dwHeight;

	NewRenderTargetDesc.dwSize = sizeof(DDSURFACEDESC);
	NewRenderTargetDesc.dwFlags = DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH ;
	NewRenderTargetDesc.ddsCaps.dwCaps = DDSCAPS_3DDEVICE | DDSCAPS_VIDEOMEMORY | DDSCAPS_LOCALVIDMEM |  DDSCAPS_TEXTURE;
	NewRenderTargetDesc.dwWidth = dwWidth;
	NewRenderTargetDesc.dwHeight = dwHeight;

	pD3DInterface->GetDDInterface()->CreateSurface(&NewRenderTargetDesc, (LPDIRECTDRAWSURFACE*)&pNewLeftRenderTarget, NULL);
	pD3DInterface->GetDDInterface()->CreateSurface(&NewRenderTargetDesc, (LPDIRECTDRAWSURFACE*)&pNewRightRenderTarget, NULL);

	LPVOID pTemp;
	pNewLeftRenderTarget->QueryInterface(IID_IDirectDrawSurface3, &pTemp);
	pNewLeftRenderTarget->Release();
	pNewLeftRenderTarget = (BaseDirectDrawSurface3*)pTemp;

	pNewRightRenderTarget->QueryInterface(IID_IDirectDrawSurface3, &pTemp);
	pNewRightRenderTarget->Release();
	pNewRightRenderTarget = (BaseDirectDrawSurface3*)pTemp;

	pNewLeftRenderTarget->QueryInterface(IID_IDirect3DTexture2, (LPVOID*)&pRTLeftTexture);
	pNewRightRenderTarget->QueryInterface(IID_IDirect3DTexture2, (LPVOID*)&pRTRightTexture);
	
	pRTLeftTexture->GetHandle(this, &RTLeftTHandle);
	pRTRightTexture->GetHandle(this, &RTRightTHandle);
		
	if (auto hr = HRESULT{ BaseDirect3DDevice2::SetRenderTarget((LPDIRECTDRAWSURFACE)pNewLeftRenderTarget, 0) }; FAILED(hr)) {
		DDSURFACEDESC ZBufferDesc;
		ZeroMemory(&ZBufferDesc, sizeof(DDSURFACEDESC));

		ZBufferDesc.dwSize = sizeof(DDSURFACEDESC);
		ZBufferDesc.ddsCaps.dwCaps = DDSCAPS_ZBUFFER ;
		pOldRenderTarget->GetAttachedSurface(&ZBufferDesc.ddsCaps, (LPDIRECTDRAWSURFACE3*)&pTemp);
		((LPDIRECTDRAWSURFACE3)pTemp)->GetSurfaceDesc(&ZBufferDesc);
		((LPDIRECTDRAWSURFACE3)pTemp)->Release();

		ZBufferDesc.dwWidth = dwWidth;
		ZBufferDesc.dwHeight = dwHeight;
		pD3DInterface->GetDDInterface()->CreateSurface(&ZBufferDesc, (LPDIRECTDRAWSURFACE*)&pLeftZBuffer, NULL);
		pD3DInterface->GetDDInterface()->CreateSurface(&ZBufferDesc, (LPDIRECTDRAWSURFACE*)&pRightZBuffer, NULL);
		
		pLeftZBuffer->QueryInterface(IID_IDirectDrawSurface3, &pTemp);
		pLeftZBuffer->Release();
		pLeftZBuffer = (BaseDirectDrawSurface3*)pTemp;

		pRightZBuffer->QueryInterface(IID_IDirectDrawSurface3, &pTemp);
		pRightZBuffer->Release();
		pRightZBuffer = (BaseDirectDrawSurface3*)pTemp;

		((BaseDirectDrawSurface3*)pNewLeftRenderTarget)->AddAttachedSurface((LPDIRECTDRAWSURFACE3)pLeftZBuffer);
		((BaseDirectDrawSurface3*)pNewRightRenderTarget)->AddAttachedSurface((LPDIRECTDRAWSURFACE3)pRightZBuffer);

		hr = BaseDirect3DDevice2::SetRenderTarget((LPDIRECTDRAWSURFACE)pNewLeftRenderTarget, 0);
	}


	m_fAspectratio = 4.0f/3.0f;
	m_fProj_y = 2.41421356f; //45deg
	m_fProj_x = m_fProj_y/m_fAspectratio;
	m_fScale_c = 10734.775019f;

	auto m_fLeftRightCameraDistance = float {config.leftright_cameradistance};
	auto fNearplane = float {100.0f};
	auto fFarplane = float {1000000.0f};
	auto fScreensize_mm = float {config.screensize};
	auto fLensoffset_from_screencenter_mm = float {config.lensoffset_from_center};

	D3DXMatrixPerspectiveFovLH(&m_ProjMatrixLeft, float {M_PI_4}, 1 / m_fAspectratio, fNearplane, fFarplane);
	m_ProjMatrixLeft.m20 = (4.0f * fLensoffset_from_screencenter_mm / fScreensize_mm);
	  
	D3DXMatrixPerspectiveFovLH(&m_ProjMatrixRight, float {M_PI_4}, 1 / m_fAspectratio, fNearplane, fFarplane);
	m_ProjMatrixRight.m20 = -(4.0f * fLensoffset_from_screencenter_mm / fScreensize_mm);

	m_ViewMatrixLeft;
	m_ViewMatrixRight;

	D3DXMatrixIdentity(&m_ViewMatrixLeft);
	m_ViewMatrixLeft.m30 = m_fLeftRightCameraDistance;

	D3DXMatrixIdentity(&m_ViewMatrixRight);
	m_ViewMatrixRight.m30 = -m_fLeftRightCameraDistance;

	D3DXMATRIX InverseViewProj;
	InverseViewProj.m00 = 2 * m_fScale_c / (dwWidth*m_fProj_x);
	InverseViewProj.m01 = 0;
	InverseViewProj.m02 = 0;
	InverseViewProj.m03 = 0;
	InverseViewProj.m10 = 0;
	InverseViewProj.m11 = -2 * m_fScale_c / (dwHeight*m_fProj_y);
	InverseViewProj.m12 = 0;
	InverseViewProj.m13 = 0;
	InverseViewProj.m20 = -m_fScale_c / m_fProj_x;
	InverseViewProj.m21 = m_fScale_c / m_fProj_y;
	InverseViewProj.m22 = m_fScale_c;
	InverseViewProj.m23 = 0;
	InverseViewProj.m30 = 0;
	InverseViewProj.m31 = 0;
	InverseViewProj.m32 = 0;
	InverseViewProj.m33 = 1;

	m_TransformMatrixLeft  = InverseViewProj * m_ViewMatrixLeft  * m_ProjMatrixLeft;
	m_TransformMatrixRight = InverseViewProj * m_ViewMatrixRight * m_ProjMatrixRight;

	m_ViewportLeft.dwSize = sizeof(D3DVIEWPORT2);
	m_ViewportLeft.dvClipHeight = 2;
	m_ViewportLeft.dvClipWidth = 2;
	m_ViewportLeft.dvClipX = -1;
	m_ViewportLeft.dvClipY = 1;
	m_ViewportLeft.dvMinZ = 0;
	m_ViewportLeft.dvMaxZ = 1;
	m_ViewportLeft.dwWidth = (DWORD) dwHeight * m_fAspectratio;	
	m_ViewportLeft.dwHeight = dwHeight;							
	m_ViewportLeft.dwX = 0;
	m_ViewportLeft.dwY = 0;

	m_ViewportRight = m_ViewportLeft;

	CreateDistortionSurface(VertexBufferLeft, 21, 21, 0);
	CreateDistortionSurface(VertexBufferRight, 21, 21, 1);
}

ProxyDirect3DDevice2::~ProxyDirect3DDevice2()
{
#ifdef SHOW_PROXYCALLS
	OutputDebugString((GetTabFill() + "ProxyDirect3DDevice2 Destructor").c_str());
#endif
	pRTLeftTexture->Release();
	pRTRightTexture->Release();
	if (pLeftZBuffer) {
		HRESULT hr = pNewLeftRenderTarget->DeleteAttachedSurface(0, pLeftZBuffer);
		if (FAILED(hr))
			pLeftZBuffer->Release();
		pLeftZBuffer->Release();
	}
	if (pRightZBuffer) {
		HRESULT hr = pNewRightRenderTarget->DeleteAttachedSurface(0, pRightZBuffer);
		if (FAILED(hr))
			pRightZBuffer->Release();
		pRightZBuffer->Release();
	}
	pNewLeftRenderTarget->Release();
	pNewRightRenderTarget->Release();
}

HRESULT WINAPI ProxyDirect3DDevice2::DrawPrimitive(D3DPRIMITIVETYPE a, D3DVERTEXTYPE b, LPVOID c, DWORD d, DWORD e)
{
#ifdef SHOW_PROXYCALLS
	OutputDebugString((GetTabFill() + "ProxyDirect3DDevice2 DrawPrimitive").c_str());
#endif
	
	if (b == D3DVT_TLVERTEX) {
		  		  
		  if (BUFFERSIZE < d) {
			  //OutputDebugString((GetTabFill() + "Vertexbuffer too small...primitive not drawn").c_str());
			  OutputDebugString("Vertexbuffer too small...primitive not drawn");
			  return DD_OK;
		  }
		  
		  D3DTLVERTEX vertexbuffer[BUFFERSIZE];
		  memcpy(vertexbuffer, c, sizeof(D3DTLVERTEX)*d);

		  //HUD Menu changes
		  if (vertexbuffer[0].dvRHW > 100) {
			  
			  //HUD/Menu Transformed&Lit Vertices to Untransformed&Lit
			  for (unsigned int i = 0; i < d; i++) {
				  vertexbuffer[i].dvSX = (vertexbuffer[i].dvSX/((float)dwWidth/2) - 1)*m_fScale_c/(vertexbuffer[i].dvRHW*m_fProj_x);
				  vertexbuffer[i].dvSY = (1 - vertexbuffer[i].dvSY/((float)dwHeight/2))*m_fScale_c/(vertexbuffer[i].dvRHW*m_fProj_y);
				  vertexbuffer[i].dvSZ = m_fScale_c/vertexbuffer[i].dvRHW;
				  vertexbuffer[i].dvRHW = 0;
			  }

			  //HUD/Menu setup (move outer parts closer to center)
			  for (unsigned int i = 0; i < d; i++) {
					if (vertexbuffer[i].dvSY < -37.0f)
						vertexbuffer[i].dvSY = vertexbuffer[i].dvSY + 4.0f;
					else if (vertexbuffer[i].dvSY > 42.0f) {
						vertexbuffer[i].dvSY = vertexbuffer[i].dvSY - 60.0f;
						vertexbuffer[i].dvRHW = 1; //Mark vertex as Health/O2 display
					}
					else if ((vertexbuffer[i].dvSY > 35.0f)&&(vertexbuffer[i].dvSY < 42.0f))
						vertexbuffer[i].dvSY = vertexbuffer[i].dvSY - 20.0f;
			  
					if (vertexbuffer[i].dvSX < -40.0f)
						vertexbuffer[i].dvSX = vertexbuffer[i].dvSX + 40.0f;
					else if (vertexbuffer[i].dvSX > 40.0f)
						vertexbuffer[i].dvSX = vertexbuffer[i].dvSX - 40.0f;
			  }


			  //HUD/Menu stereodistance & render
			  for (unsigned int i = 0; i < d; i++) {
				  if (vertexbuffer[i].dvRHW == 1) {
					  vertexbuffer[i].dvSX = vertexbuffer[i].dvSX + 1.0f; //stereodistance for Health/O2 display is 2;
					  continue;
				  }
				  vertexbuffer[i].dvSX = vertexbuffer[i].dvSX + 7.0f;  //for all other elements the stereodistance is 14;
			  }
			  BaseDirect3DDevice2::SetRenderTarget((LPDIRECTDRAWSURFACE)pNewLeftRenderTarget, 0);
			  BaseDirect3DDevice2::SetTransform(D3DTRANSFORMSTATE_PROJECTION, (D3DMATRIX*)&m_ProjMatrixLeft);
			  pCurrentDirect3DViewport2->SetViewport2(&m_ViewportLeft);
			  HRESULT hrmenu = BaseDirect3DDevice2::DrawPrimitive(a, D3DVT_LVERTEX, &vertexbuffer[0], d, e);

			  for (unsigned int i = 0; i < d; i++) {
				  if (vertexbuffer[i].dvRHW == 1) {
					  vertexbuffer[i].dvSX = vertexbuffer[i].dvSX - 2.0f;
					  continue;
				  }
				  vertexbuffer[i].dvSX = vertexbuffer[i].dvSX - 14.0f;
			  }
			  BaseDirect3DDevice2::SetRenderTarget((LPDIRECTDRAWSURFACE)pNewRightRenderTarget, 0);
			  BaseDirect3DDevice2::SetTransform(D3DTRANSFORMSTATE_PROJECTION, (D3DMATRIX*)&m_ProjMatrixRight);
			  pCurrentDirect3DViewport2->SetViewport2(&m_ViewportRight);
			  hrmenu = BaseDirect3DDevice2::DrawPrimitive(a, D3DVT_LVERTEX, &vertexbuffer[0], d, e);
			  return hrmenu;
		  }

		  //Geometry Transform
		  //Non-homogeneous to homogeneous screen space
		  for (unsigned int i = 0; i < d; i++) {
			  vertexbuffer[i].dvSX = vertexbuffer[i].dvSX/(vertexbuffer[i].dvRHW);
			  vertexbuffer[i].dvSY = vertexbuffer[i].dvSY/(vertexbuffer[i].dvRHW);
			  vertexbuffer[i].dvSZ = 1/vertexbuffer[i].dvRHW;
			  vertexbuffer[i].dvRHW = 0;
		  }

		  //Geometry backtransform to world space & render
		  //DX5 does not allow the render target to be greater than backbuffer, so the render target must to be switched between two backbuffer-sized render targets (the switching decreases the performance)
		  BaseDirect3DDevice2::SetRenderTarget((LPDIRECTDRAWSURFACE)pNewLeftRenderTarget, 0); 
		  BaseDirect3DDevice2::SetTransform(D3DTRANSFORMSTATE_PROJECTION, (D3DMATRIX*)&m_TransformMatrixLeft);
		  pCurrentDirect3DViewport2->SetViewport2(&m_ViewportLeft);
		  HRESULT hrgeometry = BaseDirect3DDevice2::DrawPrimitive(a, D3DVT_LVERTEX, &vertexbuffer[0], d, e);

		  BaseDirect3DDevice2::SetRenderTarget((LPDIRECTDRAWSURFACE)pNewRightRenderTarget, 0);
		  BaseDirect3DDevice2::SetTransform(D3DTRANSFORMSTATE_PROJECTION, (D3DMATRIX*)&m_TransformMatrixRight);
		  pCurrentDirect3DViewport2->SetViewport2(&m_ViewportRight);
		  hrgeometry = BaseDirect3DDevice2::DrawPrimitive(a, D3DVT_LVERTEX, &vertexbuffer[0], d, e);

		  return hrgeometry;
	  }

	  HRESULT hr = BaseDirect3DDevice2::DrawPrimitive(a, b, c, d, e);
	  return hr;
}

HRESULT WINAPI ProxyDirect3DDevice2::BeginScene()
{
	QueryPerformanceCounter(&t_begin);

		 
	BaseDirect3DDevice2::GetCurrentViewport(&pCurrentDirect3DViewport2);
	m_ViewportBackup.dwSize = sizeof(D3DVIEWPORT2);
	pCurrentDirect3DViewport2->GetViewport2(&m_ViewportBackup);

	D3DRECT D3DRectClear;
	D3DRectClear.x1 = 0;
	D3DRectClear.y1 = 0;
	D3DRectClear.x2 = dwWidth;
	D3DRectClear.y2 = dwHeight;

	BaseDirect3DDevice2::SetRenderTarget((LPDIRECTDRAWSURFACE)pNewLeftRenderTarget, 0);
	pCurrentDirect3DViewport2->Clear(1, &D3DRectClear, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER);

	BaseDirect3DDevice2::SetRenderTarget((LPDIRECTDRAWSURFACE)pNewRightRenderTarget, 0);
	pCurrentDirect3DViewport2->Clear(1, &D3DRectClear, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER);

	D3DMATRIX tempMatrix;
	D3DXMatrixIdentity(LPD3DXMATRIX(&tempMatrix));
	BaseDirect3DDevice2::SetTransform(D3DTRANSFORMSTATE_WORLD, &tempMatrix);
	BaseDirect3DDevice2::SetTransform(D3DTRANSFORMSTATE_VIEW, &tempMatrix);

	return BaseDirect3DDevice2::BeginScene();
}

HRESULT WINAPI ProxyDirect3DDevice2::EndScene()
{	
	HRESULT hr = BaseDirect3DDevice2::EndScene();

	QueryPerformanceCounter(&t_end);
	LARGE_INTEGER t_frequency;
	QueryPerformanceFrequency(&t_frequency);
	double  t_elapsed = ((t_begin.QuadPart - t_end.QuadPart) * 1000.0f / t_frequency.QuadPart);
	char buff[64];
	sprintf_s(buff, "Begin-End-Scene: %f", t_elapsed);
	//OutputDebugString(buff);

	if (!bSphereCreated) {

		bSphereCreated = TRUE;
	}
	
	DWORD RenderStateTextureMagBackup;
	DWORD RenderStateTextureMinBackup;

	BaseDirect3DDevice2::GetRenderState(D3DRENDERSTATE_TEXTUREMAG, &RenderStateTextureMagBackup);
	BaseDirect3DDevice2::GetRenderState(D3DRENDERSTATE_TEXTUREMIN, &RenderStateTextureMinBackup);
	BaseDirect3DDevice2::SetRenderState(D3DRENDERSTATE_TEXTUREMAG, D3DFILTER_LINEAR);
	BaseDirect3DDevice2::SetRenderState(D3DRENDERSTATE_TEXTUREMIN, D3DFILTER_LINEAR);

	BaseDirect3DDevice2::SetRenderTarget((LPDIRECTDRAWSURFACE)pOldRenderTarget, 0);
	pCurrentDirect3DViewport2->SetViewport2(&m_ViewportBackup);
	D3DRECT D3DRectClear;
	D3DRectClear.x1 = 0;
	D3DRectClear.y1 = 0;
	D3DRectClear.x2 = dwWidth;
	D3DRectClear.y2 = dwHeight;
	pCurrentDirect3DViewport2->Clear(1, &D3DRectClear,  D3DCLEAR_ZBUFFER | D3DCLEAR_TARGET );

	BaseDirect3DDevice2::BeginScene();
	BaseDirect3DDevice2::SetRenderState(D3DRENDERSTATE_TEXTUREHANDLE, RTLeftTHandle);
	BaseDirect3DDevice2::DrawPrimitive(D3DPT_TRIANGLELIST, D3DVT_TLVERTEX, &VertexBufferLeft[0], VertexBufferLeft.size(), 0);
	BaseDirect3DDevice2::SetRenderState(D3DRENDERSTATE_TEXTUREHANDLE, RTRightTHandle);
	BaseDirect3DDevice2::DrawPrimitive(D3DPT_TRIANGLELIST, D3DVT_TLVERTEX, &VertexBufferRight[0], VertexBufferRight.size(), 0);

	BaseDirect3DDevice2::EndScene();

	BaseDirect3DDevice2::SetRenderState(D3DRENDERSTATE_TEXTUREMAG, RenderStateTextureMagBackup);
	BaseDirect3DDevice2::SetRenderState(D3DRENDERSTATE_TEXTUREMIN, RenderStateTextureMinBackup);



	return hr;
}


void ProxyDirect3DDevice2::CreateDistortionSurface(std::vector<D3DTLVERTEX>& VertexBuffer, UINT NumXSlices, UINT NumYSlices, bool bLeftRight)
{
	float LensCenterX = 4.0f * config.lensoffset_from_center/config.screensize;
	int LeftRightMirror;

	if(!bLeftRight)
		LeftRightMirror = 1; 
	else
		LeftRightMirror = -1;

	float x_offset = -1.0f - LeftRightMirror*LensCenterX;
	float y_offset = -1.0f/(2*dwHeight*m_fAspectratio/dwWidth);
	float delta_x = 2.0f/((float)NumYSlices-1);
	float delta_y = (2.0f/(2*dwHeight*m_fAspectratio/dwWidth))/((float)NumXSlices-1);
	
	float OutputScale = 0.7f;
	float c0 = 0.48944684812974f;
	float c2 = 0.068051274096461f;
	float z_offset = 3.036148342835637f;
	float texture_scaleX = 2*dwWidth/(dwHeight*m_fAspectratio); //Not all pixels of the render target are used as texture
	float texture_scaleY = 2*dwHeight*m_fAspectratio/dwWidth;

	D3DTLVERTEX tempVertex;
	unsigned int pos = 0;
	unsigned int SpecularColor = 0x00000000;
	unsigned int TexelColor = 0xFFFFFFFF;
	for (unsigned int i = 0; i < NumXSlices-1 ; i++) {
		for (unsigned int j = 0; j < NumYSlices-1 ; j++) {

			float rSqrx0y0 = ((j*delta_x  + x_offset)*(j*delta_x  + x_offset) + (i*delta_y + y_offset)*(i*delta_y + y_offset));
			float rSqrx1y0 = (((j+1)*delta_x  + x_offset)*((j+1)*delta_x  + x_offset) + (i*delta_y + y_offset)*(i*delta_y + y_offset));
			float rSqrx0y1 = ((j*delta_x  + x_offset)*(j*delta_x  + x_offset) + ((i+1)*delta_y + y_offset)*((i+1)*delta_y + y_offset));
			float rSqrx1y1 = (((j+1)*delta_x  + x_offset)*((j+1)*delta_x + x_offset) + ((i+1)*delta_y + y_offset)*((i+1)*delta_y + y_offset));

			float distx0y0 = (1/OutputScale)*(c0 + (1/OutputScale)*(1/OutputScale)*c2*rSqrx0y0);
			float distx1y0 = (1/OutputScale)*(c0 + (1/OutputScale)*(1/OutputScale)*c2*rSqrx1y0);
			float distx0y1 = (1/OutputScale)*(c0 + (1/OutputScale)*(1/OutputScale)*c2*rSqrx0y1);
			float distx1y1 = (1/OutputScale)*(c0 + (1/OutputScale)*(1/OutputScale)*c2*rSqrx1y1);

			tempVertex.dvSX = (j*delta_x + x_offset);
			tempVertex.dvSY = -(i*delta_y + y_offset);
			tempVertex.dvSZ = z_offset*OutputScale-1/distx0y0;
			tempVertex.dvRHW = 0;
			tempVertex.dcColor = TexelColor;
			tempVertex.dcSpecular = SpecularColor;
			tempVertex.dvTU = (j*delta_x)/texture_scaleX;
			tempVertex.dvTV = texture_scaleY*(i*delta_y)/2;
			VertexBuffer.push_back(tempVertex);
			
			tempVertex.dvSX = ((j+1)*delta_x + x_offset);
			tempVertex.dvSY = -((i+1)*delta_y + y_offset);
			tempVertex.dvSZ = z_offset*OutputScale-1/distx1y1;
			tempVertex.dvRHW = 0;
			tempVertex.dcColor = TexelColor;
			tempVertex.dcSpecular = SpecularColor;
			tempVertex.dvTU = ((j+1)*delta_x)/texture_scaleX;
			tempVertex.dvTV = texture_scaleY*((i+1)*delta_y)/2;
			VertexBuffer.push_back(tempVertex);
					
			tempVertex.dvSX = (j*delta_x + x_offset);
			tempVertex.dvSY = -((i+1)*delta_y + y_offset);
			tempVertex.dvSZ = z_offset*OutputScale-1/distx0y1;
			tempVertex.dvRHW = 0;
			tempVertex.dcColor = TexelColor;
			tempVertex.dcSpecular = SpecularColor;
			tempVertex.dvTU = (j*delta_x)/texture_scaleX;
			tempVertex.dvTV = texture_scaleY*((i+1)*delta_y)/2;
			VertexBuffer.push_back(tempVertex);
			
			tempVertex.dvSX = (j*delta_x + x_offset);
			tempVertex.dvSY = -(i*delta_y + y_offset);
			tempVertex.dvSZ = z_offset*OutputScale-1/distx0y0;
			tempVertex.dvRHW = 0;
			tempVertex.dcColor = TexelColor;
			tempVertex.dcSpecular = SpecularColor;
			tempVertex.dvTU = (j*delta_x)/texture_scaleX;
			tempVertex.dvTV = texture_scaleY*(i*delta_y)/2;
			VertexBuffer.push_back(tempVertex);
			
			tempVertex.dvSX = ((j+1)*delta_x + x_offset);
			tempVertex.dvSY = -(i*delta_y + y_offset);
			tempVertex.dvSZ = z_offset*OutputScale-1/distx1y0;
			tempVertex.dvRHW = 0;
			tempVertex.dcColor = TexelColor;
			tempVertex.dcSpecular = SpecularColor;
			tempVertex.dvTU = ((j+1)*delta_x)/texture_scaleX;
			tempVertex.dvTV = texture_scaleY*(i*delta_y)/2;
			VertexBuffer.push_back(tempVertex);
			
			tempVertex.dvSX = ((j+1)*delta_x + x_offset);
			tempVertex.dvSY = -((i+1)*delta_y + y_offset);
			tempVertex.dvSZ = z_offset*OutputScale-1/distx1y1;
			tempVertex.dvRHW = 0;
			tempVertex.dcColor = TexelColor;
			tempVertex.dcSpecular = SpecularColor;
			tempVertex.dvTU = ((j+1)*delta_x)/texture_scaleX;
			tempVertex.dvTV = texture_scaleY*((i+1)*delta_y)/2;
			VertexBuffer.push_back(tempVertex);
		}
	}

	for (unsigned int i = 0; i < VertexBuffer.size(); i++) {
		VertexBuffer[i].dvSX = 1000.0f*VertexBuffer[i].dvSX;
		VertexBuffer[i].dvSY = 1000.0f*VertexBuffer[i].dvSY;
		VertexBuffer[i].dvSZ = 1000.0f*VertexBuffer[i].dvSZ; 
	}

	float nearplane = 100.0f;
	float farplane = 1000000.0f;

	//Projection
	for (unsigned int i = 0; i < VertexBuffer.size(); i++) {
		VertexBuffer[i].dvSX = 1*VertexBuffer[i].dvSX + LeftRightMirror*LensCenterX*VertexBuffer[i].dvSZ;
		VertexBuffer[i].dvSY = 1*VertexBuffer[i].dvSY;
		VertexBuffer[i].dvRHW = VertexBuffer[i].dvSZ;
		VertexBuffer[i].dvSZ = VertexBuffer[i].dvSZ*farplane/(farplane-nearplane) - farplane*nearplane/(farplane-nearplane);
	}

	//ViewportScale
	for (unsigned int i = 0; i < VertexBuffer.size(); i++) {
		VertexBuffer[i].dvSX = VertexBuffer[i].dvSX*dwWidth/4 + VertexBuffer[i].dvRHW*((LeftRightMirror+4)%4)*dwWidth/4; // +RHW*1/4*dwWidth for left, +RHW*3/4*dwWidth for right
		VertexBuffer[i].dvSY = -VertexBuffer[i].dvSY*dwHeight/2 + VertexBuffer[i].dvRHW*dwHeight/2;
	}

	//Transform to ScreenCoordinates
	for (unsigned int i = 0; i < VertexBuffer.size(); i++) {
		VertexBuffer[i].dvSX = VertexBuffer[i].dvSX/VertexBuffer[i].dvRHW;
		VertexBuffer[i].dvSY = VertexBuffer[i].dvSY/VertexBuffer[i].dvRHW;
		VertexBuffer[i].dvSZ = VertexBuffer[i].dvSZ/VertexBuffer[i].dvRHW;
		VertexBuffer[i].dvRHW = 1/VertexBuffer[i].dvRHW;
	}
}