#define _CRT_SECURE_NO_WARNINGS

#include <memory>
#include <sstream>
#include <vector>
#include <iostream>
#include <fstream>
#include <tuple>
#include "main.h"


// Function pointer trypedefs

typedef HRESULT (WINAPI *DirectDrawCreateProc) (GUID FAR *lpGUID, LPDIRECTDRAW FAR *lplpDD, IUnknown FAR *pUnkOuter);
typedef HRESULT (WINAPI *DirectDrawCreateExProc) (GUID FAR * lpGuid, LPVOID  *lplpDD, REFIID  iid,IUnknown FAR *pUnkOuter );
typedef HRESULT (WINAPI *DirectDrawEnumerateAProc) (LPDDENUMCALLBACKA lpCallback, LPVOID lpContext );
typedef void (WINAPI *DirectDrawOtherProc) ();

// Globals from ddraw.dll
HMODULE g_hDll = NULL;
DirectDrawCreateProc g_pfnDirectDrawCreate = NULL;
DirectDrawCreateExProc g_pfnDirectDrawCreateEx = NULL;
DirectDrawEnumerateAProc g_pfnDirectDrawEnumerateA = NULL;
DirectDrawOtherProc g_pfnAcquireDDThreadLock = NULL;
DirectDrawOtherProc g_pfnD3DParseUnknownCommand = NULL;
DirectDrawOtherProc g_pfnDDInternalLock = NULL;
DirectDrawOtherProc g_pfnDDInternalUnlock = NULL;
DirectDrawOtherProc g_pfnReleaseDDThreadLock = NULL;

const std::string config_filename = "TR2VRSettings.txt";
DLLConfig config;
OriginalToWrapperMap original_to_wrapper_map;


bool load_config(DLLConfig* pconfig) {
	auto file = std::ifstream {config_filename};
	auto file_content = std::stringstream {};
	if (file) {
		file_content << file.rdbuf() << "\n";
		file.close();
	}

	const auto key_words = std::vector<std::string> { "DDRAWPATH", "SCREENSIZE_MM", "LENSOFFSETFROMSCREENCENTER_MM", "LEFTRIGHTCAMERADISTANCE" };
	
	auto parsed_keys = std::map<std::string, std::string> {};
	
	for ( auto [line, key_words_found] = std::tuple{ std::string{}, std::vector<std::string>{key_words} }; auto result = !std::getline(file_content, line).eof();) {
		std::istringstream linestream(line);
		std::vector<std::string> parts(std::istream_iterator<std::string>{linestream}, std::istream_iterator<std::string>());
		if (parts.at(0) == "#") {
			continue;
		}

		if (parts.size() != 2) {
			OutputDebugString("config file: parameter value pair incomplete");
			break;
		}

		if (auto position = std::find(key_words_found.begin(), key_words_found.end(), parts.at(0)); position != key_words_found.end()) {
			parsed_keys.emplace(*position, parts.at(1));
			position->clear();
		}
		else {
			OutputDebugString("config file: not enough parameters");
			break;
		}
	}
	

	if (parsed_keys.size() != key_words.size()) {
		for (auto it = parsed_keys.begin(); it != parsed_keys.end(); it++) {
			OutputDebugString((it->first + " ;; " + it->second).c_str());
		}
		return false;
	}
	
	config.dllpath = parsed_keys.find(key_words.at(0))->second;
	config.screensize = std::stof(parsed_keys.find(key_words.at(1))->second);
	config.lensoffset_from_center = std::stof(parsed_keys.find(key_words.at(2))->second);
	config.leftright_cameradistance = std::stof(parsed_keys.find(key_words.at(3))->second);

	if (config.screensize > 200) { config.screensize = 200; }
	if (config.screensize < 100) { config.screensize = 100; }
	if (config.lensoffset_from_center < -5) { config.lensoffset_from_center = -5; }
	if (config.lensoffset_from_center > 5) { config.lensoffset_from_center = 5; }
	if (config.leftright_cameradistance > 200) { config.leftright_cameradistance = 200; }
	if (config.leftright_cameradistance < 0) { config.leftright_cameradistance = 0; }

	return true;
};

bool _stdcall DllMain(HANDLE, DWORD dwReason, LPVOID) {
	if(dwReason==DLL_PROCESS_ATTACH) 
	{
		//InitializeCriticalSection(&gCS);
	} else if(dwReason==DLL_PROCESS_DETACH) {
		//d3d9Exit();
		//DeleteCriticalSection(&gCS);
	}
	return true;
}


static bool LoadDll()
{
	if (g_hDll)
		return true;

	if (!load_config(&config))
		return false;

	g_hDll = LoadLibrary(config.dllpath.c_str());

	if(!g_hDll) 
	{
		OutputDebugString("ddraw.dll not found");
	}

	// Get function addresses
	g_pfnDirectDrawCreate = (DirectDrawCreateProc)GetProcAddress(g_hDll, "DirectDrawCreate");
	if(!g_pfnDirectDrawCreate)
	{
		FreeLibrary(g_hDll);
		return false;
	}

	g_pfnDirectDrawCreateEx = (DirectDrawCreateExProc)GetProcAddress(g_hDll, "DirectDrawCreateEx");
	g_pfnDirectDrawEnumerateA = (DirectDrawEnumerateAProc)GetProcAddress(g_hDll, "DirectDrawEnumerateA");
	g_pfnAcquireDDThreadLock = (DirectDrawOtherProc)GetProcAddress(g_hDll, "AcquireDDThreadLock");
	g_pfnD3DParseUnknownCommand = (DirectDrawOtherProc)GetProcAddress(g_hDll, "D3DParseUnknownCommand");
	g_pfnDDInternalLock = (DirectDrawOtherProc)GetProcAddress(g_hDll, "DDInternalLock");
	g_pfnDDInternalUnlock = (DirectDrawOtherProc)GetProcAddress(g_hDll, "DDInternalUnlock");
	g_pfnReleaseDDThreadLock = (DirectDrawOtherProc)GetProcAddress(g_hDll, "ReleaseDDThreadLock");


	// Done
	return true;
}

HRESULT WINAPI DirectDrawCreate (GUID FAR *lpGUID, LPDIRECTDRAW FAR *lplpDD, IUnknown FAR *pUnkOuter)
{
	if(!LoadDll())
		return NULL;
	
	HRESULT hr = g_pfnDirectDrawCreate(lpGUID, lplpDD, pUnkOuter);
	if (FAILED(hr))
		return hr;

	OutputDebugString("Create BaseDirectDraw");

	auto pValuePair = original_to_wrapper_map.find(*lplpDD);
	if (pValuePair != original_to_wrapper_map.end())
	{
		*lplpDD = std::get<std::unique_ptr<BaseDirectDraw>>(pValuePair->second).get();
		return hr;
	}

	original_to_wrapper_map.emplace(std::make_pair(*lplpDD, std::make_unique<BaseDirectDraw>(*lplpDD, 0)));
	*lplpDD = std::get<std::unique_ptr<BaseDirectDraw>>(original_to_wrapper_map.at(*lplpDD)).get();

	return hr;
}

HRESULT WINAPI DirectDrawCreateEx (GUID FAR * lpGuid, LPVOID  *lplpDD, REFIID  iid,IUnknown FAR *pUnkOuter )
{
	if(!LoadDll())
		return NULL;
	return g_pfnDirectDrawCreateEx(lpGuid, lplpDD, iid, pUnkOuter);
}

HRESULT WINAPI DirectDrawEnumerateA (LPDDENUMCALLBACKA lpCallback, LPVOID lpContext )
{
	if(!LoadDll())
		return NULL;

	return g_pfnDirectDrawEnumerateA(lpCallback, lpContext);
}

extern "C" void __declspec(naked) AcquireDDThreadLock() 
{ 
	_asm jmp g_pfnAcquireDDThreadLock;
}

extern "C" void __declspec(naked) D3DParseUnknownCommand() 
{ 
	
	_asm jmp g_pfnD3DParseUnknownCommand;
}

extern "C" void __declspec(naked) DDInternalLock() 
{
	
	_asm jmp g_pfnDDInternalLock;
}

extern "C" void __declspec(naked) DDInternalUnlock() 
{
	
	_asm jmp g_pfnDDInternalUnlock;
}

extern "C" void __declspec(naked) ReleaseDDThreadLock() 
{
	
	_asm jmp g_pfnReleaseDDThreadLock;
}