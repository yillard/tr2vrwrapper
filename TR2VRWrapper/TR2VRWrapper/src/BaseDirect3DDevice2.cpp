#include "main.h"


BaseDirect3DDevice2::BaseDirect3DDevice2(IDirect3DDevice2 * aOriginal, unsigned int CreatorHierarchyLevel, BaseDirect3D2* pCreatedBy) :
	mOriginal(aOriginal),
	m_nHierarchyLevel(CreatorHierarchyLevel +1),
	m_pCreatedBy(pCreatedBy),
	m_nRefCount(1),
	m_strTabFill()
{
#ifdef SHOW_BASECALLS

	for (unsigned int i = 0; i < m_nHierarchyLevel; i++)
	{
		m_strTabFill = m_strTabFill + "    ";
	}

	char buff[64];
	sprintf_s(buff, "mOriginal:0x%x, wrapper:0x%x |  ", mOriginal, this);
	m_strTabFill = m_strTabFill + buff;
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 Constructor").c_str());
#endif
}

BaseDirect3DDevice2::~BaseDirect3DDevice2()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 Destructor").c_str());
	
#endif
	
	if(mOriginal)
		mOriginal->Release();
}

HRESULT WINAPI BaseDirect3DDevice2::QueryInterface(REFIID riid, LPVOID * ppvObj)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 QueryInterface").c_str());
#endif
  	HRESULT hr = mOriginal->QueryInterface(riid, ppvObj);
	if (FAILED(hr))
		return hr;

#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill  + "Found " + RefIIDToName(riid)).c_str());
#endif	
	return hr;
}

ULONG WINAPI BaseDirect3DDevice2::AddRef()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 AddRef").c_str());
#endif
	return ++m_nRefCount;
}

ULONG WINAPI BaseDirect3DDevice2::Release()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 Release").c_str());
#endif

	m_nRefCount--;
	
#ifdef SHOW_BASECALLS
	char buff[64];
	sprintf_s(buff, "RefCount:%d, 0x%x", m_nRefCount, this);
	OutputDebugString((m_strTabFill + buff).c_str());
#endif

	if (m_nRefCount == 0)
	{
		original_to_wrapper_map.erase(mOriginal);
		return 0;
	}
	return m_nRefCount;
}

HRESULT WINAPI BaseDirect3DDevice2::GetCaps(LPD3DDEVICEDESC a, LPD3DDEVICEDESC b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 GetCaps").c_str());
#endif
	return mOriginal->GetCaps(a, b);
}

HRESULT WINAPI BaseDirect3DDevice2::SwapTextureHandles(LPDIRECT3DTEXTURE2 a, LPDIRECT3DTEXTURE2 b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 SwapTextureHandles").c_str());
#endif
	return mOriginal->SwapTextureHandles((a)?((BaseDirect3DTexture2*)a)->GetOriginal():0, (b)?((BaseDirect3DTexture2*)b)->GetOriginal():0);
}

HRESULT WINAPI BaseDirect3DDevice2::GetStats(LPD3DSTATS a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 GetStats").c_str());
#endif
	return mOriginal->GetStats(a);
}

HRESULT WINAPI BaseDirect3DDevice2::AddViewport(LPDIRECT3DVIEWPORT2 a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 AddViewport").c_str());
#endif
  //a->mOriginal instead of a if Direct3DViewport redirected
	return mOriginal->AddViewport(a);
}

HRESULT WINAPI BaseDirect3DDevice2::DeleteViewport(LPDIRECT3DVIEWPORT2 a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 DeleteViewport").c_str());
#endif
  //a->mOriginal instead of a if Direct3DViewport redirected
	return mOriginal->DeleteViewport(a);
}

HRESULT WINAPI BaseDirect3DDevice2::NextViewport(LPDIRECT3DVIEWPORT2 a, LPDIRECT3DVIEWPORT2 * b, DWORD c)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 NextViewport").c_str());
#endif
  //a->mOriginal instead of a if Direct3DViewport redirected
	return mOriginal->NextViewport(a, b, c);
}

HRESULT WINAPI BaseDirect3DDevice2::EnumTextureFormats(LPD3DENUMTEXTUREFORMATSCALLBACK a, LPVOID b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 EnumTextureFormats").c_str());
#endif
	return mOriginal->EnumTextureFormats(a, b);
}

HRESULT WINAPI BaseDirect3DDevice2::BeginScene()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 BeginScene").c_str());
#endif
	return mOriginal->BeginScene();
}

HRESULT WINAPI BaseDirect3DDevice2::EndScene()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 EndScene").c_str());
#endif
	return mOriginal->EndScene();
}

HRESULT WINAPI BaseDirect3DDevice2::GetDirect3D(LPDIRECT3D2 * a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 GetDirect3D").c_str());
#endif
	if (!m_pCreatedBy)
		return D3DERR_INVALID_DEVICE;
	else {
		*a = m_pCreatedBy;
		return D3D_OK;
	}
}

HRESULT WINAPI BaseDirect3DDevice2::SetCurrentViewport(LPDIRECT3DVIEWPORT2 a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 SetCurrentViewport").c_str());
#endif
  //a->mOriginal instead of a if Direct3DViewport redirected
	return mOriginal->SetCurrentViewport(a);
}

HRESULT WINAPI BaseDirect3DDevice2::GetCurrentViewport(LPDIRECT3DVIEWPORT2 * a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 GetCurrentViewport").c_str());
#endif
	return mOriginal->GetCurrentViewport(a);
}

HRESULT WINAPI BaseDirect3DDevice2::SetRenderTarget(LPDIRECTDRAWSURFACE a, DWORD b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 SetRenderTarget").c_str());
#endif
	return mOriginal->SetRenderTarget((a)?((BaseDirectDrawSurface*)a)->GetOriginal():0, b);
}

HRESULT WINAPI BaseDirect3DDevice2::GetRenderTarget(LPDIRECTDRAWSURFACE * a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 GetRenderTarget").c_str());
#endif
  HRESULT hr = mOriginal->GetRenderTarget(a);
  if(FAILED(hr))
	  return hr;
  
  auto pValuePair = original_to_wrapper_map.find(*a);
  if(pValuePair != original_to_wrapper_map.end())
  {
	  *a = std::get< std::unique_ptr<BaseDirectDrawSurface>>(pValuePair->second).get();
	  return hr;
  }

  original_to_wrapper_map.emplace(std::make_pair(*a, std::make_unique<BaseDirectDrawSurface>(*a, m_nHierarchyLevel)));
  *a = std::get< std::unique_ptr<BaseDirectDrawSurface>>(original_to_wrapper_map.at(*a)).get();

  return hr;
}

HRESULT WINAPI BaseDirect3DDevice2::Begin(D3DPRIMITIVETYPE a, D3DVERTEXTYPE b, DWORD c)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 Begin").c_str());
#endif
	return mOriginal->Begin(a, b, c);
}

HRESULT WINAPI BaseDirect3DDevice2::BeginIndexed(D3DPRIMITIVETYPE a, D3DVERTEXTYPE b, LPVOID c, DWORD d, DWORD e)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 BeginIndexed").c_str());
#endif
	return mOriginal->BeginIndexed(a, b, c, d, e);
}

HRESULT WINAPI BaseDirect3DDevice2::Vertex(LPVOID a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 Vertex").c_str());
#endif
	return mOriginal->Vertex(a);
}

HRESULT WINAPI BaseDirect3DDevice2::Index(WORD a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 Index").c_str());
#endif
	return mOriginal->Index(a);
}

HRESULT WINAPI BaseDirect3DDevice2::End(DWORD a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 End").c_str());
#endif
	return mOriginal->End(a);
}

HRESULT WINAPI BaseDirect3DDevice2::GetRenderState(D3DRENDERSTATETYPE a, LPDWORD b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 GetRenderState").c_str());
#endif
	return mOriginal->GetRenderState(a, b);
}

HRESULT WINAPI BaseDirect3DDevice2::SetRenderState(D3DRENDERSTATETYPE a, DWORD b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 SetRenderState").c_str());
#endif
	return mOriginal->SetRenderState(a, b);
}

HRESULT WINAPI BaseDirect3DDevice2::GetLightState(D3DLIGHTSTATETYPE a, LPDWORD b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 GetLightState").c_str());
#endif
	return mOriginal->GetLightState(a, b);
}

HRESULT WINAPI BaseDirect3DDevice2::SetLightState(D3DLIGHTSTATETYPE a, DWORD b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 SetLightState").c_str());
#endif
	return mOriginal->SetLightState(a, b);
}

HRESULT WINAPI BaseDirect3DDevice2::SetTransform(D3DTRANSFORMSTATETYPE a, LPD3DMATRIX b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 SetTransform").c_str());
#endif
	return mOriginal->SetTransform(a, b);
}

HRESULT WINAPI BaseDirect3DDevice2::GetTransform(D3DTRANSFORMSTATETYPE a, LPD3DMATRIX b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 GetTransform").c_str());
#endif
	return mOriginal->GetTransform(a, b);
}

HRESULT WINAPI BaseDirect3DDevice2::MultiplyTransform(D3DTRANSFORMSTATETYPE a, LPD3DMATRIX b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 MultiplyTransform").c_str());
#endif
	return mOriginal->MultiplyTransform(a, b);
}

HRESULT WINAPI BaseDirect3DDevice2::DrawPrimitive(D3DPRIMITIVETYPE a, D3DVERTEXTYPE b, LPVOID c, DWORD d, DWORD e)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 DrawPrimitive").c_str());
#endif
	return mOriginal->DrawPrimitive(a, b, c, d, e);
}

HRESULT WINAPI BaseDirect3DDevice2::DrawIndexedPrimitive(D3DPRIMITIVETYPE a, D3DVERTEXTYPE b, LPVOID c, DWORD d, LPWORD e, DWORD f, DWORD g)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 DrawIndexedPrimitive").c_str());
#endif
	return mOriginal->DrawIndexedPrimitive(a, b, c, d, e, f, g);
}

HRESULT WINAPI BaseDirect3DDevice2::SetClipStatus(LPD3DCLIPSTATUS a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 SetClipStatus").c_str());
#endif
	return mOriginal->SetClipStatus(a);
}

HRESULT WINAPI BaseDirect3DDevice2::GetClipStatus(LPD3DCLIPSTATUS a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DDevice2 GetClipStatus").c_str());
#endif
	return mOriginal->GetClipStatus(a);
}

IDirect3DDevice2* BaseDirect3DDevice2::GetOriginal()
{
	return mOriginal;
}

std::string BaseDirect3DDevice2::GetTabFill()
{
	return m_strTabFill;
}