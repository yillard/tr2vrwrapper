#include "main.h"


BaseDirectDrawSurface::BaseDirectDrawSurface(IDirectDrawSurface * aOriginal, unsigned int CreatorHierarchyLevel) :
	mOriginal(aOriginal),
	m_nRefCount(1),
	m_nHierarchyLevel(CreatorHierarchyLevel + 1),
	m_strTabFill()

{
#ifdef SHOW_BASECALLS
	
	for (unsigned int i = 0; i < m_nHierarchyLevel; i++)
	{
		m_strTabFill = m_strTabFill + "    ";
	}

	char buff[64];
	sprintf_s(buff, "mOriginal:0x%x, wrapper:0x%x |  ", mOriginal, this);
	m_strTabFill = m_strTabFill + buff;
	OutputDebugString((m_strTabFill + "BaseDirectDrawSurface Constructor").c_str());
#endif
}

BaseDirectDrawSurface::~BaseDirectDrawSurface()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDrawSurface Destructor").c_str());
	
#endif
	
	if(mOriginal)
		mOriginal->Release();
}

HRESULT __stdcall BaseDirectDrawSurface::QueryInterface(REFIID riid, LPVOID FAR * ppvObj)
{
#ifdef SHOW_BASECALLS
  	OutputDebugString((m_strTabFill + "BaseDirectDrawSurface QueryInterface").c_str());
#endif
	HRESULT hr = mOriginal->QueryInterface(riid, ppvObj);
	if (FAILED(hr))
		return hr;

#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill  + "Found " + RefIIDToName(riid)).c_str());
#endif

	if (riid == IID_IDirectDrawSurface3)
	{
#ifdef SHOW_BASECALLS
		OutputDebugString((m_strTabFill + "Create BaseDirectDrawSurface3").c_str());
#endif

		auto pValuePair = original_to_wrapper_map.find(static_cast<LPDIRECTDRAWSURFACE3>(*ppvObj));
		if (pValuePair != original_to_wrapper_map.end())
		{
			*ppvObj = std::get<std::unique_ptr<BaseDirectDrawSurface3>>(pValuePair->second).get(); //Proxy crashes -> Proxy not needed
			return hr;
		}

		original_to_wrapper_map.emplace(std::make_pair(static_cast<LPDIRECTDRAWSURFACE3>(*ppvObj), std::make_unique<BaseDirectDrawSurface3>(static_cast<LPDIRECTDRAWSURFACE3>(*ppvObj), m_nHierarchyLevel))); //Proxy crashes -> Proxy not needed
		*ppvObj = std::get<std::unique_ptr<BaseDirectDrawSurface3>>(original_to_wrapper_map.at(static_cast<LPDIRECTDRAWSURFACE3>(*ppvObj))).get(); //Proxy crashes -> Proxy not needed
	}
		
	return hr;
}

ULONG __stdcall BaseDirectDrawSurface::AddRef()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDrawSurface AddRef").c_str());
#endif
	return ++m_nRefCount;
}

ULONG __stdcall BaseDirectDrawSurface::Release()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDrawSurface Release").c_str());
#endif

	m_nRefCount--;

#ifdef SHOW_BASECALLS
	char buff[64];
	sprintf_s(buff, "RefCount:%d, 0x%x", m_nRefCount, this);
	OutputDebugString((m_strTabFill + buff).c_str());
#endif

	if (m_nRefCount == 0)
	{
		original_to_wrapper_map.erase(mOriginal);
		return 0;
	}
	return m_nRefCount;
}

HRESULT __stdcall BaseDirectDrawSurface::AddAttachedSurface(LPDIRECTDRAWSURFACE a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface AddAttachedSurface").c_str());
#endif
  return mOriginal->AddAttachedSurface((a)?((BaseDirectDrawSurface*)a)->GetOriginal():0);
}

HRESULT __stdcall BaseDirectDrawSurface::AddOverlayDirtyRect(LPRECT a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface AddOverlayDirtyRect").c_str());
#endif
  return mOriginal->AddOverlayDirtyRect(a);
}

HRESULT __stdcall BaseDirectDrawSurface::Blt(LPRECT a, LPDIRECTDRAWSURFACE b, LPRECT c, DWORD d, LPDDBLTFX e)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface Blt").c_str());
#endif
  return mOriginal->Blt(a, (b)?((BaseDirectDrawSurface*)b)->GetOriginal():0, c, d, e);

}

HRESULT __stdcall BaseDirectDrawSurface::BltBatch(LPDDBLTBATCH a, DWORD b, DWORD c)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface BltBatch").c_str());
#endif
  return mOriginal->BltBatch(a, b, c);
}

HRESULT __stdcall BaseDirectDrawSurface::BltFast(DWORD a, DWORD b, LPDIRECTDRAWSURFACE c, LPRECT d, DWORD e)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface BltFast").c_str());
#endif
  return mOriginal->BltFast(a, b, (c)?((BaseDirectDrawSurface*)c)->GetOriginal():0, d, e);
}

HRESULT __stdcall BaseDirectDrawSurface::DeleteAttachedSurface(DWORD a, LPDIRECTDRAWSURFACE b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface DeleteAttachedSurface").c_str());
#endif
  return mOriginal->DeleteAttachedSurface(a, (b)?((BaseDirectDrawSurface*)b)->GetOriginal():0);
}

HRESULT __stdcall BaseDirectDrawSurface::EnumAttachedSurfaces(LPVOID a, LPDDENUMSURFACESCALLBACK b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface EnumAttachedSurfaces").c_str());
#endif
  return mOriginal->EnumAttachedSurfaces(a, b);
}

HRESULT __stdcall BaseDirectDrawSurface::EnumOverlayZOrders(DWORD a, LPVOID b, LPDDENUMSURFACESCALLBACK c)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface EnumOverlayZOrders").c_str());
#endif
  return mOriginal->EnumOverlayZOrders(a, b, c);
}

HRESULT __stdcall BaseDirectDrawSurface::Flip(LPDIRECTDRAWSURFACE a, DWORD b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface Flip").c_str());
#endif
  return mOriginal->Flip((a)?((BaseDirectDrawSurface*)a)->GetOriginal():0, b);
}

HRESULT __stdcall BaseDirectDrawSurface::GetAttachedSurface(LPDDSCAPS a, LPDIRECTDRAWSURFACE FAR * b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface GetAttachedSurface").c_str());
#endif
  HRESULT hr = mOriginal->GetAttachedSurface(a, b);
  if (FAILED(hr))
	  return hr;

  auto pValuePair = original_to_wrapper_map.find(*b);
  if (pValuePair != original_to_wrapper_map.end())
  {
	  *b = std::get<std::unique_ptr<BaseDirectDrawSurface>>(pValuePair->second).get();
	  return hr;
  }

  original_to_wrapper_map.emplace(std::make_pair(*b, std::make_unique<BaseDirectDrawSurface>(*b, m_nHierarchyLevel)));
  *b = std::get<std::unique_ptr<BaseDirectDrawSurface>>(original_to_wrapper_map.at(*b)).get();

  return hr;
}

HRESULT __stdcall BaseDirectDrawSurface::GetBltStatus(DWORD a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface GetBltStatus").c_str());
#endif
  return mOriginal->GetBltStatus(a);
}

HRESULT __stdcall BaseDirectDrawSurface::GetCaps(LPDDSCAPS a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface GetCaps").c_str());
#endif
  return mOriginal->GetCaps(a);
}

HRESULT __stdcall BaseDirectDrawSurface::GetClipper(LPDIRECTDRAWCLIPPER FAR * a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface GetClipper").c_str());
#endif
  return mOriginal->GetClipper(a);
}

HRESULT __stdcall BaseDirectDrawSurface::GetColorKey(DWORD a, LPDDCOLORKEY b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface GetColorKey").c_str());
#endif
  return mOriginal->GetColorKey(a, b);
}

HRESULT __stdcall BaseDirectDrawSurface::GetDC(HDC FAR * a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface GetDC").c_str());
#endif
  return mOriginal->GetDC(a);
}

HRESULT __stdcall BaseDirectDrawSurface::GetFlipStatus(DWORD a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface GetFlipStatus").c_str());
#endif
  return mOriginal->GetFlipStatus(a);
}

HRESULT __stdcall BaseDirectDrawSurface::GetOverlayPosition(LPLONG a, LPLONG b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface GetOverlayPosition").c_str());
#endif
  return mOriginal->GetOverlayPosition(a, b);
}

HRESULT __stdcall BaseDirectDrawSurface::GetPalette(LPDIRECTDRAWPALETTE FAR * a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface GetPalette").c_str());
#endif
  return mOriginal->GetPalette(a);
}

HRESULT __stdcall BaseDirectDrawSurface::GetPixelFormat(LPDDPIXELFORMAT a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface GetPixelFormat").c_str());
#endif
  return mOriginal->GetPixelFormat(a);
}

HRESULT __stdcall BaseDirectDrawSurface::GetSurfaceDesc(LPDDSURFACEDESC a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface GetSurfaceDesc").c_str());
#endif
  return mOriginal->GetSurfaceDesc(a);
}

HRESULT __stdcall BaseDirectDrawSurface::Initialize(LPDIRECTDRAW a, LPDDSURFACEDESC b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface Initialize").c_str());
#endif
  return  DDERR_ALREADYINITIALIZED;
}

HRESULT __stdcall BaseDirectDrawSurface::IsLost()
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface IsLost").c_str());
#endif
  return mOriginal->IsLost();
}

HRESULT __stdcall BaseDirectDrawSurface::Lock(LPRECT a, LPDDSURFACEDESC b, DWORD c, HANDLE d)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface Lock").c_str());
#endif
  return mOriginal->Lock(a, b, c, d);
}

HRESULT __stdcall BaseDirectDrawSurface::ReleaseDC(HDC a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface ReleaseDC").c_str());
#endif
  return mOriginal->ReleaseDC(a);
}

HRESULT __stdcall BaseDirectDrawSurface::Restore()
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface Restore").c_str());
#endif
  return mOriginal->Restore();
}

HRESULT __stdcall BaseDirectDrawSurface::SetClipper(LPDIRECTDRAWCLIPPER a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface SetClipper").c_str());
#endif
  return mOriginal->SetClipper(a);
}

HRESULT __stdcall BaseDirectDrawSurface::SetColorKey(DWORD a, LPDDCOLORKEY b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface SetColorKey").c_str());
#endif
  return mOriginal->SetColorKey(a, b);
}

HRESULT __stdcall BaseDirectDrawSurface::SetOverlayPosition(LONG a, LONG b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface SetOverlayPosition").c_str());
#endif
  return mOriginal->SetOverlayPosition(a, b);
}

HRESULT __stdcall BaseDirectDrawSurface::SetPalette(LPDIRECTDRAWPALETTE a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface SetPalette").c_str());
#endif
  return mOriginal->SetPalette(a);
}

HRESULT __stdcall BaseDirectDrawSurface::Unlock(LPVOID a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface Unlock").c_str());
#endif
  return mOriginal->Unlock(a);
}

HRESULT __stdcall BaseDirectDrawSurface::UpdateOverlay(LPRECT a, LPDIRECTDRAWSURFACE b, LPRECT c, DWORD d, LPDDOVERLAYFX e)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface UpdateOverlay").c_str());
#endif
  return mOriginal->UpdateOverlay(a, (b)?((BaseDirectDrawSurface *)b)->GetOriginal():0, c, d, e);
}

HRESULT __stdcall BaseDirectDrawSurface::UpdateOverlayDisplay(DWORD a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface UpdateOverlayDisplay").c_str());
#endif
  return mOriginal->UpdateOverlayDisplay(a);
}

HRESULT __stdcall BaseDirectDrawSurface::UpdateOverlayZOrder(DWORD a, LPDIRECTDRAWSURFACE b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDrawSurface UpdateOverlayZOrder").c_str());
#endif
  return mOriginal->UpdateOverlayZOrder(a, (b)?((BaseDirectDrawSurface *)b)->GetOriginal():0);
}

IDirectDrawSurface* BaseDirectDrawSurface::GetOriginal()
{
	return mOriginal;
}
