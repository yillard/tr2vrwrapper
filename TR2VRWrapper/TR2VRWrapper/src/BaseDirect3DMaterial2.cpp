#include "main.h"


BaseDirect3DMaterial2::BaseDirect3DMaterial2(IDirect3DMaterial2 * aOriginal, unsigned int CreatorHierarchyLevel) :
	mOriginal(aOriginal),
	m_nRefCount(1),
	m_nHierarchyLevel(CreatorHierarchyLevel +1),
	m_strTabFill()

{
#ifdef SHOW_BASECALLS
	
	for (unsigned int i = 0; i < m_nHierarchyLevel; i++)
	{
		m_strTabFill = m_strTabFill + "    ";
	}

	char buff[64];
	sprintf_s(buff, "mOriginal:0x%x, wrapper:0x%x |  ", mOriginal, this);
	m_strTabFill = m_strTabFill + buff;
	OutputDebugString((m_strTabFill + "BaseDirect3DMaterial2 Constructor").c_str());
#endif
}

BaseDirect3DMaterial2::~BaseDirect3DMaterial2()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DMaterial2 Destructor").c_str());
	
#endif
	
	if(mOriginal)
		mOriginal->Release();
}

HRESULT __stdcall BaseDirect3DMaterial2::QueryInterface(REFIID riid, LPVOID * ppvObj)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DMaterial2 QueryInterface").c_str());
#endif
  	HRESULT hr = mOriginal->QueryInterface(riid, ppvObj);
	if (FAILED(hr))
		return hr;

#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill  + "Found " + RefIIDToName(riid)).c_str());
#endif	
	return hr;
}

ULONG __stdcall BaseDirect3DMaterial2::AddRef()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DMaterial2 AddRef").c_str());
#endif
	return ++m_nRefCount;
}

ULONG __stdcall BaseDirect3DMaterial2::Release()
{
#ifdef SHOW_BASECALLS
 	OutputDebugString((m_strTabFill + "BaseDirect3DMaterial2 Release").c_str());
#endif

	m_nRefCount--;

#ifdef SHOW_BASECALLS
	char buff[64];
	sprintf_s(buff, "RefCount:%d, 0x%x", m_nRefCount, this);
	OutputDebugString((m_strTabFill + buff).c_str());
#endif

	if (m_nRefCount == 0)
	{
		original_to_wrapper_map.erase(mOriginal);
		return 0;
	}
	return m_nRefCount;
}

HRESULT __stdcall BaseDirect3DMaterial2::SetMaterial(LPD3DMATERIAL a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirect3DMaterial2 SetMaterial").c_str());
#endif
  return mOriginal->SetMaterial(a);
}

HRESULT __stdcall BaseDirect3DMaterial2::GetMaterial(LPD3DMATERIAL a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirect3DMaterial2 GetMaterial").c_str());
#endif
  return mOriginal->GetMaterial(a);
}

HRESULT __stdcall BaseDirect3DMaterial2::GetHandle(LPDIRECT3DDEVICE2 a, LPD3DMATERIALHANDLE b)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirect3DMaterial2 GetHandle").c_str());
#endif
  return mOriginal->GetHandle((a)?((BaseDirect3DDevice2*)a)->GetOriginal():0, b);
}