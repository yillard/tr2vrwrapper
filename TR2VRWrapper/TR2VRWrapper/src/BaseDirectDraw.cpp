#define _CRT_SECURE_NO_WARNINGS

#include "main.h"
#include "BaseDirectDraw.h"
#include "BaseDirectDraw2.h"
#include "BaseDirectDrawSurface.h"


BaseDirectDraw::BaseDirectDraw(IDirectDraw * aOriginal, unsigned int CreatorHierarchyLevel) :
  mOriginal(aOriginal),
  m_nRefCount(1),
  m_nHierarchyLevel(CreatorHierarchyLevel + 1),
  m_strTabFill()

{
#ifdef SHOW_BASECALLS
	
	for (unsigned int i = 0; i < m_nHierarchyLevel; i++)
	{
		m_strTabFill = m_strTabFill + "    ";
	}

	char buff[64];
	sprintf_s(buff, "mOriginal:0x%x, wrapper:0x%x |  ", mOriginal, this);
	m_strTabFill = m_strTabFill + buff;
	OutputDebugString((m_strTabFill + "BaseDirectDraw Constructor").c_str());
#endif
}

BaseDirectDraw::~BaseDirectDraw()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw Destructor").c_str());
	
#endif
	

#ifdef SHOW_BASECALLS
	char buff[64];
	sprintf_s(buff, "OriginalsCount:%d", original_to_wrapper_map.size());
	OutputDebugString(buff);
#endif
	if(mOriginal)
		mOriginal->Release();
}

HRESULT WINAPI BaseDirectDraw::QueryInterface(REFIID riid, LPVOID FAR * ppvObj)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw QueryInterface").c_str());
#endif
	HRESULT hr = mOriginal->QueryInterface(riid, ppvObj);
	if (FAILED(hr))
		return hr;

#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill  + "Found " + RefIIDToName(riid)).c_str());
#endif

	if (riid == IID_IDirectDraw2)
	{
#ifdef SHOW_BASECALLS
		OutputDebugString((m_strTabFill + "Create BaseDirectDraw2").c_str());
#endif
		auto pValuePair = OriginalToWrapperMap::iterator(original_to_wrapper_map.find(static_cast<LPDIRECTDRAW2>(*ppvObj)));
		if (pValuePair != original_to_wrapper_map.end())
		{
			*ppvObj = std::get<std::unique_ptr<BaseDirectDraw2>>(pValuePair->second).get();
			return hr;
		}

		original_to_wrapper_map.emplace(std::make_pair(static_cast<LPDIRECTDRAW2>(*ppvObj), std::make_unique<BaseDirectDraw2>(static_cast<LPDIRECTDRAW2>(*ppvObj), m_nHierarchyLevel)));
		*ppvObj = std::get<std::unique_ptr<BaseDirectDraw2>>(original_to_wrapper_map.at(static_cast<LPDIRECTDRAW2>(*ppvObj))).get();
	}
		
	return hr;
}

ULONG WINAPI BaseDirectDraw::AddRef()
{ 
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw AddRef").c_str());
#endif
	return ++m_nRefCount;
}

ULONG WINAPI BaseDirectDraw::Release()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw Release").c_str());
#endif

	m_nRefCount--;

#ifdef SHOW_BASECALLS
	char buff[64];
	sprintf_s(buff, "RefCount:%d, 0x%x", m_nRefCount, this);
	OutputDebugString((m_strTabFill + buff).c_str());
#endif

	if (m_nRefCount == 0)
	{
		original_to_wrapper_map.erase(mOriginal);
		return 0;
	}
	return m_nRefCount;
}

HRESULT WINAPI BaseDirectDraw::Compact()
{ 
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDraw Compact").c_str());
#endif
  return mOriginal->Compact();
}

HRESULT WINAPI BaseDirectDraw::CreateClipper(DWORD dwFlags, LPDIRECTDRAWCLIPPER FAR *lplpDDClipper, IUnknown FAR *pUnkOuter)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw CreateClipper").c_str());
#endif
	return mOriginal->CreateClipper(dwFlags, lplpDDClipper, pUnkOuter);
}

HRESULT WINAPI BaseDirectDraw::CreatePalette(DWORD dwFlags, LPPALETTEENTRY lpDDColorArray, LPDIRECTDRAWPALETTE FAR *lplpDDPalette, IUnknown FAR *pUnkOuter)
{ 
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw CreatePalette").c_str());
#endif
	return mOriginal->CreatePalette(dwFlags, lpDDColorArray, lplpDDPalette, pUnkOuter);
}

HRESULT WINAPI BaseDirectDraw::CreateSurface(LPDDSURFACEDESC lpDDSurfaceDesc, LPDIRECTDRAWSURFACE FAR *lplpDDSurface, IUnknown FAR *pUnkOuter)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw CreateSurface").c_str());
#endif
  	HRESULT hr = mOriginal->CreateSurface(lpDDSurfaceDesc, lplpDDSurface, pUnkOuter);
	if(FAILED(hr))
		return hr;

	auto pValuePair = original_to_wrapper_map.find(*lplpDDSurface);
	if (pValuePair != original_to_wrapper_map.end())
	{
		*lplpDDSurface = std::get<std::unique_ptr<BaseDirectDrawSurface>>(pValuePair->second).get();
		return hr;
	}

	original_to_wrapper_map.emplace(std::make_pair(*lplpDDSurface, std::make_unique<BaseDirectDrawSurface>(*lplpDDSurface, m_nHierarchyLevel)));
	*lplpDDSurface = std::get<std::unique_ptr<BaseDirectDrawSurface>>(original_to_wrapper_map.at(*lplpDDSurface)).get();

	return hr;
}

HRESULT WINAPI BaseDirectDraw::DuplicateSurface(LPDIRECTDRAWSURFACE lpDDSurface, LPDIRECTDRAWSURFACE FAR *lplpDupDDSurface)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw DuplicateSurface").c_str());
#endif
	HRESULT hr = mOriginal->DuplicateSurface((lpDDSurface)?((BaseDirectDrawSurface*)lpDDSurface)->GetOriginal():0, lplpDupDDSurface);

	auto pValuePair = original_to_wrapper_map.find(*lplpDupDDSurface);
	if (pValuePair != original_to_wrapper_map.end())
	{
		*lplpDupDDSurface = std::get<std::unique_ptr<BaseDirectDrawSurface>>(pValuePair->second).get();
		return hr;
	}

	original_to_wrapper_map.emplace(std::make_pair(*lplpDupDDSurface, std::make_unique<BaseDirectDrawSurface>(*lplpDupDDSurface, m_nHierarchyLevel)));
	*lplpDupDDSurface = std::get<std::unique_ptr<BaseDirectDrawSurface>>(original_to_wrapper_map.at(*lplpDupDDSurface)).get();

	return  hr;
}

HRESULT WINAPI BaseDirectDraw::EnumDisplayModes(DWORD a, LPDDSURFACEDESC b, LPVOID c, LPDDENUMMODESCALLBACK d)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw EnumDisplayModes").c_str());
#endif
	return mOriginal->EnumDisplayModes(a, b, c, d);
}

HRESULT WINAPI BaseDirectDraw::EnumSurfaces(DWORD a, LPDDSURFACEDESC b, LPVOID c, LPDDENUMSURFACESCALLBACK d)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw EnumSurfaces").c_str());
#endif
	return mOriginal->EnumSurfaces(a, b, c, d);
}

HRESULT WINAPI BaseDirectDraw::FlipToGDISurface()
{ 
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw FlipToGDISurface").c_str());
#endif
	return mOriginal->FlipToGDISurface();
}

HRESULT WINAPI BaseDirectDraw::GetCaps(LPDDCAPS a, LPDDCAPS b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw GetCaps").c_str());
#endif
	return mOriginal->GetCaps(a, b);
}

HRESULT WINAPI BaseDirectDraw::GetDisplayMode(LPDDSURFACEDESC a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw GetDisplayMode").c_str());
#endif
	return mOriginal->GetDisplayMode(a);
}

HRESULT WINAPI BaseDirectDraw::GetFourCCCodes(LPDWORD a, LPDWORD b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw GetFourCCCodes").c_str());
#endif
	return mOriginal->GetFourCCCodes(a, b);
}

HRESULT WINAPI BaseDirectDraw::GetGDISurface(LPDIRECTDRAWSURFACE FAR * a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDraw GetGDISurface").c_str());
#endif
  HRESULT hr = mOriginal->GetGDISurface(a);

  auto pValuePair = original_to_wrapper_map.find(*a);
  if(pValuePair != original_to_wrapper_map.end())
  {
	  *a = std::get<std::unique_ptr<BaseDirectDrawSurface>>(pValuePair->second).get();
	  return hr;
  }

  original_to_wrapper_map.emplace(std::make_pair(*a, std::make_unique<BaseDirectDrawSurface>(*a, m_nHierarchyLevel)));
  *a = std::get<std::unique_ptr<BaseDirectDrawSurface>>(original_to_wrapper_map.at(*a)).get();

  return hr;
}

HRESULT WINAPI BaseDirectDraw::GetMonitorFrequency(LPDWORD a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw GetMonitorFrequency").c_str());
#endif
	return mOriginal->GetMonitorFrequency(a);
}

HRESULT WINAPI BaseDirectDraw::GetScanLine(LPDWORD a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw GetScanLine").c_str());
#endif
	return mOriginal->GetScanLine(a);
}

HRESULT WINAPI BaseDirectDraw::GetVerticalBlankStatus(LPBOOL a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw GetVerticalBlankStatus").c_str());
#endif
	return mOriginal->GetVerticalBlankStatus(a);
}

HRESULT WINAPI BaseDirectDraw::Initialize(GUID FAR * a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw Initialize").c_str());
#endif
	return mOriginal->Initialize(a);
}

HRESULT WINAPI BaseDirectDraw::RestoreDisplayMode()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw RestoreDisplayMode").c_str());
#endif
	return mOriginal->RestoreDisplayMode();
}

HRESULT WINAPI BaseDirectDraw::SetCooperativeLevel(HWND a, DWORD b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw SetCooperativeLevel").c_str());
#endif
	return mOriginal->SetCooperativeLevel(a, b);
}

HRESULT WINAPI BaseDirectDraw::SetDisplayMode(DWORD a, DWORD b, DWORD c)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw SetDisplayMode").c_str());
#endif
	return mOriginal->SetDisplayMode(a, b, c);
}

HRESULT WINAPI BaseDirectDraw::WaitForVerticalBlank(DWORD a, HANDLE b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw WaitForVerticalBlank").c_str());
#endif
	return mOriginal->WaitForVerticalBlank(a, b);
}