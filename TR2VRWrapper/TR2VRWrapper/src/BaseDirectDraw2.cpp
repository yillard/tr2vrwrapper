#include "main.h"


BaseDirectDraw2::BaseDirectDraw2(IDirectDraw2 * aOriginal, unsigned int CreatorHierarchyLevel):
	mOriginal(aOriginal),
	m_nRefCount(1),
	m_nHierarchyLevel(CreatorHierarchyLevel + 1),
	m_strTabFill()

{
#ifdef SHOW_BASECALLS
	
	for (unsigned int i = 0; i < m_nHierarchyLevel; i++)
	{
		m_strTabFill = m_strTabFill + "    ";
	}

	char buff[64];
	sprintf_s(buff, "mOriginal:0x%x, wrapper:0x%x |  ", mOriginal, this);
	m_strTabFill = m_strTabFill + buff;
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 Constructor").c_str());
#endif
}

BaseDirectDraw2::~BaseDirectDraw2()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 Destructor").c_str());
	
#endif
	
	if(mOriginal)
		mOriginal->Release();
}

HRESULT WINAPI BaseDirectDraw2::QueryInterface(REFIID riid, LPVOID FAR * ppvObj)
{
#ifdef SHOW_BASECALLS
  	OutputDebugString((m_strTabFill + "BaseDirectDraw2 QueryInterface").c_str());
#endif
	HRESULT hr = mOriginal->QueryInterface(riid, ppvObj);
	if (FAILED(hr))
		return hr;

#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill  + "Found " + RefIIDToName(riid)).c_str());
#endif

	if (riid == IID_IDirect3D2)
	{
#ifdef SHOW_BASECALLS
		OutputDebugString((m_strTabFill + "Create BaseDirect3D2").c_str());
#endif
		
		auto pValuePair = original_to_wrapper_map.find(static_cast<LPDIRECT3D2>(*ppvObj));
		if (pValuePair != original_to_wrapper_map.end())
		{
			*ppvObj = std::get< std::unique_ptr<BaseDirect3D2>>(pValuePair->second).get();
			return hr;
		}

		original_to_wrapper_map.emplace(std::make_pair(static_cast<LPDIRECT3D2>(*ppvObj), std::make_unique<BaseDirect3D2>(static_cast<LPDIRECT3D2>(*ppvObj), m_nHierarchyLevel, this)));
		*ppvObj = std::get< std::unique_ptr<BaseDirect3D2>>(original_to_wrapper_map.at(static_cast<LPDIRECT3D2>(*ppvObj))).get();
	}
		
	return hr;
}

ULONG WINAPI BaseDirectDraw2::AddRef()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 AddRef").c_str());
#endif
	return ++m_nRefCount;
}

ULONG WINAPI BaseDirectDraw2::Release()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 Release").c_str());
#endif

	m_nRefCount--;

#ifdef SHOW_BASECALLS
	char buff[64];
	sprintf_s(buff, "RefCount:%d, 0x%x", m_nRefCount, this);
	OutputDebugString((m_strTabFill + buff).c_str());
#endif

	if (m_nRefCount == 0)
	{
		original_to_wrapper_map.erase(mOriginal);
		return 0;
	}
	return m_nRefCount;
}

HRESULT WINAPI BaseDirectDraw2::Compact()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 Compact").c_str());
#endif
	return mOriginal->Compact();
}

HRESULT WINAPI BaseDirectDraw2::CreateClipper(DWORD a, LPDIRECTDRAWCLIPPER FAR * b, IUnknown FAR * c)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 CreateClipper").c_str());
#endif
	return mOriginal->CreateClipper(a, b, c);
}

HRESULT WINAPI BaseDirectDraw2::CreatePalette(DWORD a, LPPALETTEENTRY b, LPDIRECTDRAWPALETTE FAR * c, IUnknown FAR * d)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 CreatePalette").c_str());
#endif
	return mOriginal->CreatePalette(a, b, c, d);
}

HRESULT WINAPI BaseDirectDraw2::CreateSurface(LPDDSURFACEDESC a, LPDIRECTDRAWSURFACE FAR * b, IUnknown FAR * c)
{
#ifdef SHOW_BASECALLS
    OutputDebugString((m_strTabFill + "BaseDirectDraw2 CreateSurface").c_str());
#endif
	HRESULT hr = mOriginal->CreateSurface(a, b, c);
	if(FAILED(hr))
		return hr;

	auto pValuePair = original_to_wrapper_map.find(*b);
	if (pValuePair != original_to_wrapper_map.end())
	{
		*b = std::get< std::unique_ptr<BaseDirectDrawSurface>>(pValuePair->second).get();
		return hr;
	}

	original_to_wrapper_map.emplace(std::make_pair(*b, std::make_unique<BaseDirectDrawSurface>(*b, m_nHierarchyLevel)));
	*b = std::get< std::unique_ptr<BaseDirectDrawSurface>>(original_to_wrapper_map.at(*b)).get();

	return hr;
}

HRESULT WINAPI BaseDirectDraw2::DuplicateSurface(LPDIRECTDRAWSURFACE a, LPDIRECTDRAWSURFACE FAR * b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 DuplicateSurface").c_str());
#endif
	HRESULT hr = mOriginal->DuplicateSurface((a)?((BaseDirectDrawSurface*)a)->GetOriginal():0, b);

	auto pValuePair = original_to_wrapper_map.find(*b);
	if (pValuePair != original_to_wrapper_map.end())
	{
		*b = std::get<std::unique_ptr<BaseDirectDrawSurface>>(pValuePair->second).get();
		return hr;
	}

	original_to_wrapper_map.emplace(std::make_pair(*b, std::make_unique<BaseDirectDrawSurface>(*b, m_nHierarchyLevel)));
	*b = std::get<std::unique_ptr<BaseDirectDrawSurface>>(original_to_wrapper_map.at(*b)).get();

	return  hr;
}

HRESULT WINAPI BaseDirectDraw2::EnumDisplayModes(DWORD a, LPDDSURFACEDESC b, LPVOID c, LPDDENUMMODESCALLBACK d)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 EnumDisplayModes").c_str());
#endif
	return mOriginal->EnumDisplayModes(a, b, c, d);
}

HRESULT WINAPI BaseDirectDraw2::EnumSurfaces(DWORD a, LPDDSURFACEDESC b, LPVOID c, LPDDENUMSURFACESCALLBACK d)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 EnumSurfaces").c_str());
#endif
	return mOriginal->EnumSurfaces(a, b, c, d);
}

HRESULT WINAPI BaseDirectDraw2::FlipToGDISurface()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 FlipToGDISurface").c_str());
#endif
	return mOriginal->FlipToGDISurface();
}

HRESULT WINAPI BaseDirectDraw2::GetCaps(LPDDCAPS a, LPDDCAPS b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 GetCaps").c_str());
#endif
	return mOriginal->GetCaps(a, b);
}

HRESULT WINAPI BaseDirectDraw2::GetDisplayMode(LPDDSURFACEDESC a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 GetDisplayMode").c_str());
#endif
	return mOriginal->GetDisplayMode(a);
}

HRESULT WINAPI BaseDirectDraw2::GetFourCCCodes(LPDWORD a, LPDWORD b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 GetFourCCCodes").c_str());
#endif
	return mOriginal->GetFourCCCodes(a, b);
}

HRESULT WINAPI BaseDirectDraw2::GetGDISurface(LPDIRECTDRAWSURFACE FAR * a)
{
#ifdef SHOW_BASECALLS
  OutputDebugString((m_strTabFill + "BaseDirectDraw2 GetGDISurface").c_str());
#endif
  HRESULT hr = mOriginal->GetGDISurface(a);

  auto pValuePair = original_to_wrapper_map.find(*a);
  if(pValuePair != original_to_wrapper_map.end())
  {
	  *a = std::get<std::unique_ptr<BaseDirectDrawSurface>>(pValuePair->second).get();
	  return hr;
  }

  original_to_wrapper_map.emplace(std::make_pair(*a, std::make_unique<BaseDirectDrawSurface>(*a, m_nHierarchyLevel)));
  *a = std::get<std::unique_ptr<BaseDirectDrawSurface>>(original_to_wrapper_map.at(*a)).get();

  return hr;
}

HRESULT WINAPI BaseDirectDraw2::GetMonitorFrequency(LPDWORD a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 GetMonitorFrequency").c_str());
#endif
	return mOriginal->GetMonitorFrequency(a);
}

HRESULT WINAPI BaseDirectDraw2::GetScanLine(LPDWORD a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 GetScanLine").c_str());
#endif
	return mOriginal->GetScanLine(a);
}

HRESULT WINAPI BaseDirectDraw2::GetVerticalBlankStatus(LPBOOL a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 GetVerticalBlankStatus").c_str());
#endif
	return mOriginal->GetVerticalBlankStatus(a);
}

HRESULT WINAPI BaseDirectDraw2::Initialize(GUID FAR * a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 Initialize").c_str());
#endif
	return mOriginal->Initialize(a);
}

HRESULT WINAPI BaseDirectDraw2::RestoreDisplayMode()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 RestoreDisplayMode").c_str());
#endif
	return mOriginal->RestoreDisplayMode();
}

HRESULT WINAPI BaseDirectDraw2::SetCooperativeLevel(HWND a, DWORD b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 SetCooperativeLevel").c_str());
#endif
	return mOriginal->SetCooperativeLevel(a, b);
}

HRESULT WINAPI BaseDirectDraw2::SetDisplayMode(DWORD a, DWORD b, DWORD c, DWORD d, DWORD e)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 SetDisplayMode").c_str());
#endif
	return mOriginal->SetDisplayMode(a, b, c, d, e);
}

HRESULT WINAPI BaseDirectDraw2::WaitForVerticalBlank(DWORD a, HANDLE b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 WaitForVerticalBlank").c_str());
#endif
	return mOriginal->WaitForVerticalBlank(a, b);
}

HRESULT WINAPI BaseDirectDraw2::GetAvailableVidMem(LPDDSCAPS a, LPDWORD b, LPDWORD c)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirectDraw2 GetAvailableVidMem").c_str());
#endif
	return mOriginal->GetAvailableVidMem(a, b, c);
}

void BaseDirectDraw2::SetRenderTargetPointer (LPDIRECTDRAWSURFACE3 pRenderTarget)
{
	m_RenderTargetPointer = pRenderTarget;
}

LPDIRECTDRAWSURFACE3 BaseDirectDraw2::GetRenderTargetPointer()
{
	return m_RenderTargetPointer;
}