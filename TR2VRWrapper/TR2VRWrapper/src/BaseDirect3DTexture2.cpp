#include "main.h"


BaseDirect3DTexture2::BaseDirect3DTexture2(IDirect3DTexture2 * aOriginal, unsigned int CreatorHierarchyLevel) :
	mOriginal(aOriginal),
	m_nRefCount(1),
	m_nHierarchyLevel(CreatorHierarchyLevel + 1),
	m_strTabFill()

{
#ifdef SHOW_BASECALLS
	
	for (unsigned int i = 0; i < m_nHierarchyLevel; i++)
	{
		m_strTabFill = m_strTabFill + "    ";
	}

	char buff[64];
	sprintf_s(buff, "mOriginal:0x%x, wrapper:0x%x |  ", mOriginal, this);
	m_strTabFill = m_strTabFill + buff;
	OutputDebugString((m_strTabFill + "BaseDirect3DTexture2 Constructor").c_str());
#endif
}

BaseDirect3DTexture2::~BaseDirect3DTexture2()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DTexture2 Destructor").c_str());
	
#endif
	
	if(mOriginal)
		mOriginal->Release();
}

HRESULT __stdcall BaseDirect3DTexture2::QueryInterface(REFIID riid, LPVOID * ppvObj)
{
#ifdef SHOW_BASECALLS
  	OutputDebugString((m_strTabFill + "BaseDirect3DTexture2 QueryInterface").c_str());
#endif
	HRESULT hr = mOriginal->QueryInterface(riid, ppvObj);
	if (FAILED(hr))
		return hr;

#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill  + "Found " + RefIIDToName(riid)).c_str());
#endif
		
	return hr;
}

ULONG __stdcall BaseDirect3DTexture2::AddRef()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DTexture2 AddRef").c_str());
#endif
	return ++m_nRefCount;
}

ULONG __stdcall BaseDirect3DTexture2::Release()
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DTexture2 Release").c_str());
#endif

	m_nRefCount--;

#ifdef SHOW_BASECALLS
	char buff[64];
	sprintf_s(buff, "RefCount:%d, 0x%x", m_nRefCount, this);
	OutputDebugString((m_strTabFill + buff).c_str());
#endif

	if (m_nRefCount == 0)
	{
		original_to_wrapper_map.erase(mOriginal);
		return 0;
	}
	return m_nRefCount;
}

HRESULT __stdcall BaseDirect3DTexture2::GetHandle(LPDIRECT3DDEVICE2 a, LPD3DTEXTUREHANDLE b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DTexture2 GetHandle").c_str());
#endif
	return mOriginal->GetHandle((a)?((BaseDirect3DDevice2*)a)->GetOriginal():0, b);
}

HRESULT __stdcall BaseDirect3DTexture2::PaletteChanged(DWORD a, DWORD b)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DTexture2 PaletteChanged").c_str());
#endif
	return mOriginal->PaletteChanged(a, b);
}

HRESULT __stdcall BaseDirect3DTexture2::Load(LPDIRECT3DTEXTURE2 a)
{
#ifdef SHOW_BASECALLS
	OutputDebugString((m_strTabFill + "BaseDirect3DTexture2 PaletteChanged").c_str());
#endif
	return mOriginal->Load((a)?((BaseDirect3DTexture2 *)a)->GetOriginal():0);
}

IDirect3DTexture2* BaseDirect3DTexture2::GetOriginal()
{
	return mOriginal;
}