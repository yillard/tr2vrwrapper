#ifndef PROXYDIRECT3DDEVICE2_H_INCLUDED
#define PROXYDIRECT3DDEVICE2_H_INCLUDED

#include "main.h"
#include <vector>


#define BUFFERSIZE 50

class BaseDirectDrawSurface3;
class BaseDirect3D2;

class ProxyDirect3DDevice2 : public BaseDirect3DDevice2
{
public:
	ProxyDirect3DDevice2(IDirect3DDevice2 * aOriginal, unsigned int CreatorHierarchyLevel, BaseDirect3D2* pCreatedBy);
	virtual ~ProxyDirect3DDevice2();

    virtual HRESULT WINAPI DrawPrimitive(D3DPRIMITIVETYPE a, D3DVERTEXTYPE b, LPVOID c, DWORD d, DWORD e);
	virtual HRESULT WINAPI BeginScene();
	virtual HRESULT WINAPI EndScene();

	void CreateDistortionSurface(std::vector<D3DTLVERTEX>& VertexBuffer, UINT NumXSlices, UINT NumYSegments, bool bLeftRight);

private:

	LARGE_INTEGER t_begin, t_end;

	float m_fProj_y; //45deg
	float m_fProj_x;
	float m_fScale_c;
	float m_fAspectratio;

	std::vector<D3DTLVERTEX> VertexBufferLeft;
	std::vector<D3DTLVERTEX> VertexBufferRight;
	bool bSphereCreated;

	DWORD dwWidth;
	DWORD dwHeight;
	

	LPDIRECT3DVIEWPORT2 pCurrentDirect3DViewport2;
	D3DVIEWPORT2 m_ViewportBackup;
	D3DVIEWPORT2 m_ViewportLeft;
	D3DVIEWPORT2 m_ViewportRight;
	D3DVIEWPORT2 m_ViewportRT;

	D3DXMATRIX m_ViewMatrixLeft;
	D3DXMATRIX m_ViewMatrixRight;

	D3DXMATRIX m_ProjMatrixLeft;
	D3DXMATRIX m_ProjMatrixRight;

	D3DXMATRIX m_TransformMatrixLeft;
	D3DXMATRIX m_TransformMatrixRight;

	BaseDirectDrawSurface3* pNewLeftRenderTarget;
	BaseDirectDrawSurface3* pNewRightRenderTarget;
	BaseDirectDrawSurface3* pOldRenderTarget;
	BaseDirectDrawSurface3* pLeftZBuffer;
	BaseDirectDrawSurface3* pRightZBuffer;

	BaseDirect3D2* pD3DInterface;

	D3DTEXTUREHANDLE RTLeftTHandle;
	D3DTEXTUREHANDLE RTRightTHandle;
	LPDIRECT3DTEXTURE2 pRTLeftTexture;
	LPDIRECT3DTEXTURE2 pRTRightTexture;
};




#endif