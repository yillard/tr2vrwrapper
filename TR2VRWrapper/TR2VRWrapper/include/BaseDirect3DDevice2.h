#ifndef BASEDIRECT3DDEVICE2_H_INCLUDED
#define BADEDIRECT3DDEVICE2_H_INCLUDED

#include <d3d.h>
#include <string>


class BaseDirect3DDevice2 : public IDirect3DDevice2
{
public:
	BaseDirect3DDevice2(IDirect3DDevice2 * aOriginal, unsigned int CreatorHierarchyLevel, BaseDirect3D2* pCreatedBy);
	virtual ~BaseDirect3DDevice2();

	virtual HRESULT WINAPI	QueryInterface(REFIID riid, LPVOID * ppvObj);
	virtual ULONG WINAPI	AddRef();
	virtual ULONG WINAPI	Release();
	virtual HRESULT WINAPI	GetCaps(LPD3DDEVICEDESC a, LPD3DDEVICEDESC b);
	virtual HRESULT WINAPI	SwapTextureHandles(LPDIRECT3DTEXTURE2 a, LPDIRECT3DTEXTURE2 b);
	virtual HRESULT WINAPI	GetStats(LPD3DSTATS a);
	virtual HRESULT WINAPI	AddViewport(LPDIRECT3DVIEWPORT2 a);
	virtual HRESULT WINAPI	DeleteViewport(LPDIRECT3DVIEWPORT2 a);
	virtual HRESULT WINAPI	NextViewport(LPDIRECT3DVIEWPORT2 a, LPDIRECT3DVIEWPORT2 * b, DWORD c);
	virtual HRESULT WINAPI	EnumTextureFormats(LPD3DENUMTEXTUREFORMATSCALLBACK a, LPVOID b);
	virtual HRESULT WINAPI	BeginScene();
	virtual HRESULT WINAPI	EndScene();
	virtual HRESULT WINAPI	GetDirect3D(LPDIRECT3D2 * a);
	virtual HRESULT WINAPI	SetCurrentViewport(LPDIRECT3DVIEWPORT2 a);
	virtual HRESULT WINAPI	GetCurrentViewport(LPDIRECT3DVIEWPORT2 * a);
	virtual HRESULT WINAPI	SetRenderTarget(LPDIRECTDRAWSURFACE a, DWORD b);
	virtual HRESULT WINAPI	GetRenderTarget(LPDIRECTDRAWSURFACE * a);
	virtual HRESULT WINAPI	Begin(D3DPRIMITIVETYPE a, D3DVERTEXTYPE b, DWORD c);
	virtual HRESULT WINAPI	BeginIndexed(D3DPRIMITIVETYPE a, D3DVERTEXTYPE b, LPVOID c, DWORD d, DWORD e);
	virtual HRESULT WINAPI	Vertex(LPVOID a);
	virtual HRESULT WINAPI	Index(WORD a);
	virtual HRESULT WINAPI	End(DWORD a);
	virtual HRESULT WINAPI	GetRenderState(D3DRENDERSTATETYPE a, LPDWORD b);
	virtual HRESULT WINAPI	SetRenderState(D3DRENDERSTATETYPE a, DWORD b);
	virtual HRESULT WINAPI	GetLightState(D3DLIGHTSTATETYPE a, LPDWORD b);
	virtual HRESULT WINAPI	SetLightState(D3DLIGHTSTATETYPE a, DWORD b);
	virtual HRESULT WINAPI	SetTransform(D3DTRANSFORMSTATETYPE a, LPD3DMATRIX b);
	virtual HRESULT WINAPI	GetTransform(D3DTRANSFORMSTATETYPE a, LPD3DMATRIX b);
	virtual HRESULT WINAPI	MultiplyTransform(D3DTRANSFORMSTATETYPE a, LPD3DMATRIX b);
	virtual HRESULT WINAPI	DrawPrimitive(D3DPRIMITIVETYPE a, D3DVERTEXTYPE b, LPVOID c, DWORD d, DWORD e);
	virtual HRESULT WINAPI	DrawIndexedPrimitive(D3DPRIMITIVETYPE a, D3DVERTEXTYPE b, LPVOID c, DWORD d, LPWORD e, DWORD f, DWORD g);
	virtual HRESULT WINAPI	SetClipStatus(LPD3DCLIPSTATUS a);
	virtual HRESULT WINAPI	GetClipStatus(LPD3DCLIPSTATUS a);

	IDirect3DDevice2*	GetOriginal();

protected:
	std::string GetTabFill();

private:
	IDirect3DDevice2*	mOriginal;
	BaseDirect3D2*		m_pCreatedBy;
	ULONG				m_nRefCount;
	unsigned int		m_nHierarchyLevel;
	std::string			m_strTabFill;
};




#endif