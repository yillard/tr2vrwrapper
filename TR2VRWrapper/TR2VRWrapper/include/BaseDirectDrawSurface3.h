#ifndef BASEDIRECTDRAWSURFACE3_H_INCLUDED
#define BASEDIRECTDRAWSURFACE3_H_INCLUDED

#include <ddraw.h>
#include <string>


class BaseDirectDrawSurface3 : public IDirectDrawSurface3
{
public:
	BaseDirectDrawSurface3(IDirectDrawSurface3 * aOriginal, unsigned int CreatorHierarchyLevel);
	virtual ~BaseDirectDrawSurface3();

	virtual HRESULT WINAPI	QueryInterface(REFIID riid, LPVOID FAR * ppvObj);
	virtual ULONG WINAPI	AddRef();
	virtual ULONG WINAPI	Release();
	virtual HRESULT WINAPI	AddAttachedSurface(LPDIRECTDRAWSURFACE3 a);
	virtual HRESULT WINAPI	AddOverlayDirtyRect(LPRECT a);
	virtual HRESULT WINAPI	Blt(LPRECT a, LPDIRECTDRAWSURFACE3 b, LPRECT c, DWORD d, LPDDBLTFX e);
	virtual HRESULT WINAPI	BltBatch(LPDDBLTBATCH a, DWORD b, DWORD c);
	virtual HRESULT WINAPI	BltFast(DWORD a, DWORD b, LPDIRECTDRAWSURFACE3 c, LPRECT d, DWORD e);
	virtual HRESULT WINAPI	DeleteAttachedSurface(DWORD a, LPDIRECTDRAWSURFACE3 b);
	virtual HRESULT WINAPI	EnumAttachedSurfaces(LPVOID a, LPDDENUMSURFACESCALLBACK b);
	virtual HRESULT WINAPI	EnumOverlayZOrders(DWORD a, LPVOID b, LPDDENUMSURFACESCALLBACK c);
	virtual HRESULT WINAPI	Flip(LPDIRECTDRAWSURFACE3 a, DWORD b);
	virtual HRESULT WINAPI	GetAttachedSurface(LPDDSCAPS a, LPDIRECTDRAWSURFACE3 FAR * b);
	virtual HRESULT WINAPI	GetBltStatus(DWORD a);
	virtual HRESULT WINAPI	GetCaps(LPDDSCAPS a);
	virtual HRESULT WINAPI	GetClipper(LPDIRECTDRAWCLIPPER FAR * a);
	virtual HRESULT WINAPI	GetColorKey(DWORD a, LPDDCOLORKEY b);
	virtual HRESULT WINAPI	GetDC(HDC FAR * a);
	virtual HRESULT WINAPI	GetFlipStatus(DWORD a);
	virtual HRESULT WINAPI	GetOverlayPosition(LPLONG a, LPLONG b);
	virtual HRESULT WINAPI	GetPalette(LPDIRECTDRAWPALETTE FAR * a);
	virtual HRESULT WINAPI	GetPixelFormat(LPDDPIXELFORMAT a);
	virtual HRESULT WINAPI	GetSurfaceDesc(LPDDSURFACEDESC a);
	virtual HRESULT WINAPI	Initialize(LPDIRECTDRAW a, LPDDSURFACEDESC b);
	virtual HRESULT WINAPI	IsLost();
	virtual HRESULT WINAPI	Lock(LPRECT a, LPDDSURFACEDESC b, DWORD c, HANDLE d);
	virtual HRESULT WINAPI	ReleaseDC(HDC a);
	virtual HRESULT WINAPI	Restore();
	virtual HRESULT WINAPI	SetClipper(LPDIRECTDRAWCLIPPER a);
	virtual HRESULT WINAPI	SetColorKey(DWORD a, LPDDCOLORKEY b);
	virtual HRESULT WINAPI	SetOverlayPosition(LONG a, LONG b);
	virtual HRESULT WINAPI	SetPalette(LPDIRECTDRAWPALETTE a);
	virtual HRESULT WINAPI	Unlock(LPVOID a);
	virtual HRESULT WINAPI	UpdateOverlay(LPRECT a, LPDIRECTDRAWSURFACE3 b, LPRECT c, DWORD d, LPDDOVERLAYFX e);
	virtual HRESULT WINAPI	UpdateOverlayDisplay(DWORD a);
	virtual HRESULT WINAPI	UpdateOverlayZOrder(DWORD a, LPDIRECTDRAWSURFACE3 b);
	virtual HRESULT WINAPI	GetDDInterface(LPVOID FAR * a);
	virtual HRESULT WINAPI	PageLock(DWORD a);
	virtual HRESULT WINAPI	PageUnlock(DWORD a);
	virtual HRESULT WINAPI	SetSurfaceDesc(LPDDSURFACEDESC a, DWORD b);

	IDirectDrawSurface3*	GetOriginal();
	std::string				GetTabFill();

private:
	IDirectDrawSurface3*	mOriginal;
	ULONG					m_nRefCount;
	unsigned int			m_nHierarchyLevel;
	std::string				m_strTabFill;
};


#endif