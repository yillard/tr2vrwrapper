#ifndef BASEDIRECTDRAW2_H_INCLUDED
#define BASEDIRECTDRAW2_H_INCLUDED

#include <ddraw.h>
#include <string>


class BaseDirectDraw2 : public IDirectDraw2
{
public:
	BaseDirectDraw2(IDirectDraw2 * aOriginal, unsigned int CreatorHierarchyLevel);
	virtual ~BaseDirectDraw2();

	virtual HRESULT WINAPI	QueryInterface(REFIID riid, LPVOID FAR * ppvObj);
	virtual ULONG WINAPI	AddRef();
	virtual ULONG WINAPI	Release();
	virtual HRESULT WINAPI	Compact();
	virtual HRESULT WINAPI	CreateClipper(DWORD a, LPDIRECTDRAWCLIPPER FAR * b, IUnknown FAR * c);
	virtual HRESULT WINAPI	CreatePalette(DWORD a, LPPALETTEENTRY b, LPDIRECTDRAWPALETTE FAR * c, IUnknown FAR * d);
	virtual HRESULT WINAPI	CreateSurface(LPDDSURFACEDESC a, LPDIRECTDRAWSURFACE FAR * b, IUnknown FAR * c);
	virtual HRESULT WINAPI	DuplicateSurface(LPDIRECTDRAWSURFACE a, LPDIRECTDRAWSURFACE FAR * b);
	virtual HRESULT WINAPI	EnumDisplayModes(DWORD a, LPDDSURFACEDESC b, LPVOID c, LPDDENUMMODESCALLBACK d);
	virtual HRESULT WINAPI	EnumSurfaces(DWORD a, LPDDSURFACEDESC b, LPVOID c, LPDDENUMSURFACESCALLBACK d);
	virtual HRESULT WINAPI	FlipToGDISurface();
	virtual HRESULT WINAPI	GetCaps(LPDDCAPS a, LPDDCAPS b);
	virtual HRESULT WINAPI	GetDisplayMode(LPDDSURFACEDESC a);
	virtual HRESULT WINAPI	GetFourCCCodes(LPDWORD a, LPDWORD b);
	virtual HRESULT WINAPI	GetGDISurface(LPDIRECTDRAWSURFACE FAR * a);
	virtual HRESULT WINAPI	GetMonitorFrequency(LPDWORD a);
	virtual HRESULT WINAPI	GetScanLine(LPDWORD a);
	virtual HRESULT WINAPI	GetVerticalBlankStatus(LPBOOL a);
	virtual HRESULT WINAPI	Initialize(GUID FAR * a);
	virtual HRESULT WINAPI	RestoreDisplayMode();
	virtual HRESULT WINAPI	SetCooperativeLevel(HWND a, DWORD b);
	virtual HRESULT WINAPI	SetDisplayMode(DWORD a, DWORD b, DWORD c, DWORD d, DWORD e);
	virtual HRESULT WINAPI	WaitForVerticalBlank(DWORD a, HANDLE b);
	virtual HRESULT WINAPI	GetAvailableVidMem(LPDDSCAPS a, LPDWORD b, LPDWORD c);

	void					SetRenderTargetPointer (LPDIRECTDRAWSURFACE3 pRenderTarget);
	IDirectDrawSurface3*	GetRenderTargetPointer();

private:
	IDirectDraw2*			mOriginal;
	ULONG					m_nRefCount;
	unsigned int			m_nHierarchyLevel;
	std::string				m_strTabFill;
	IDirectDrawSurface3*	m_RenderTargetPointer;
};






#endif