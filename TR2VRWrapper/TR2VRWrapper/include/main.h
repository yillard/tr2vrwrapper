#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

//#define STRICT
#define WIN32_LEAN_AND_MEAN
#define INITGUID

#define _USE_MATH_DEFINES

//#define DIRECTDRAW_VERSION 0x0700

#include <Windows.h>
#include <ddraw.h>
#include <d3d.h>
#include <d3dx.h>

#include <stdio.h>
#include <cguid.h>
#include <atlbase.h>
#include <atlconv.h>
#include <variant>
#include <memory>
#include <string>
#include <map>

#include "BaseDirectDraw2.h"
#include "BaseDirect3D2.h"
#include "BaseDirect3DDevice2.h"
#include "ProxyDirect3DDevice2.h"
#include "BaseDirectDrawSurface3.h"

#include "BaseDirect3DTexture2.h"
#include "BaseDirect3DMaterial2.h"
#include "BaseDirectDrawSurface.h"
#include "BaseDirectDraw.h"


//#define SHOW_BASECALLS
//#define SHOW_PROXYCALLS


// #define LENSOFFSETFROMSCREENCENTER_MM  ((float)SCREENSIZE_MM/4.0f - 64.0f/2.0f)//5.44 //Lens offset in mm


using DLLConfig = struct {
	std::string dllpath;
	float screensize;
	float lensoffset_from_center;
	float leftright_cameradistance;
};

extern DLLConfig config;

std::string RefIIDToName(REFIID riid);

using OriginalToWrapperMap = std::map<
	std::variant<
		LPDIRECT3D2,
		LPDIRECT3DDEVICE2,
		LPDIRECT3DMATERIAL2,
		LPDIRECT3DTEXTURE2,
		LPDIRECTDRAW,
		LPDIRECTDRAW2,
		LPDIRECTDRAWSURFACE,
		LPDIRECTDRAWSURFACE3>,
	std::variant<
		std::unique_ptr<BaseDirectDraw>,
		std::unique_ptr<BaseDirectDraw2>,
		std::unique_ptr<BaseDirectDrawSurface>,
		std::unique_ptr<BaseDirectDrawSurface3>,
		std::unique_ptr<BaseDirect3DMaterial2>,
		std::unique_ptr<BaseDirect3DTexture2>,
		std::unique_ptr<BaseDirect3D2>,
		std::unique_ptr<ProxyDirect3DDevice2>,
		std::unique_ptr<BaseDirect3DDevice2>>	
	>;

extern OriginalToWrapperMap original_to_wrapper_map;


#endif