#ifndef BASEDIRECTDRAWSURFACE_H_INCLUDED
#define BASEDIRECTDRAWSURFACE_H_INCLUDED

#include <ddraw.h>
#include <string>


class BaseDirectDrawSurface : public IDirectDrawSurface
{
public:
	BaseDirectDrawSurface(IDirectDrawSurface * aOriginal, unsigned int CreatorHierarchyLevel);
	virtual ~BaseDirectDrawSurface();

	virtual HRESULT WINAPI	QueryInterface(REFIID riid, LPVOID FAR * ppvObj);
	virtual ULONG WINAPI	AddRef();
	virtual ULONG WINAPI	Release();
	virtual HRESULT WINAPI	AddAttachedSurface(LPDIRECTDRAWSURFACE a);
	virtual HRESULT WINAPI	AddOverlayDirtyRect(LPRECT a);
	virtual HRESULT WINAPI	Blt(LPRECT a, LPDIRECTDRAWSURFACE b, LPRECT c, DWORD d, LPDDBLTFX e);
	virtual HRESULT WINAPI	BltBatch(LPDDBLTBATCH a, DWORD b, DWORD c);
	virtual HRESULT WINAPI	BltFast(DWORD a, DWORD b, LPDIRECTDRAWSURFACE c, LPRECT d, DWORD e);
	virtual HRESULT WINAPI	DeleteAttachedSurface(DWORD a, LPDIRECTDRAWSURFACE b);
	virtual HRESULT WINAPI	EnumAttachedSurfaces(LPVOID a, LPDDENUMSURFACESCALLBACK b);
	virtual HRESULT WINAPI	EnumOverlayZOrders(DWORD a, LPVOID b, LPDDENUMSURFACESCALLBACK c);
	virtual HRESULT WINAPI	Flip(LPDIRECTDRAWSURFACE a, DWORD b);
	virtual HRESULT WINAPI	GetAttachedSurface(LPDDSCAPS a, LPDIRECTDRAWSURFACE FAR * b);
	virtual HRESULT WINAPI	GetBltStatus(DWORD a);
	virtual HRESULT WINAPI	GetCaps(LPDDSCAPS a);
	virtual HRESULT WINAPI	GetClipper(LPDIRECTDRAWCLIPPER FAR * a);
	virtual HRESULT WINAPI	GetColorKey(DWORD a, LPDDCOLORKEY b);
	virtual HRESULT WINAPI	GetDC(HDC FAR * a);
	virtual HRESULT WINAPI	GetFlipStatus(DWORD a);
	virtual HRESULT WINAPI	GetOverlayPosition(LPLONG a, LPLONG b);
	virtual HRESULT WINAPI	GetPalette(LPDIRECTDRAWPALETTE FAR * a);
	virtual HRESULT WINAPI	GetPixelFormat(LPDDPIXELFORMAT a);
	virtual HRESULT WINAPI	GetSurfaceDesc(LPDDSURFACEDESC a);
	virtual HRESULT WINAPI	Initialize(LPDIRECTDRAW a, LPDDSURFACEDESC b);
	virtual HRESULT WINAPI	IsLost();
	virtual HRESULT WINAPI	Lock(LPRECT a, LPDDSURFACEDESC b, DWORD c, HANDLE d);
	virtual HRESULT WINAPI	ReleaseDC(HDC a);
	virtual HRESULT WINAPI	Restore();
	virtual HRESULT WINAPI	SetClipper(LPDIRECTDRAWCLIPPER a);
	virtual HRESULT WINAPI	SetColorKey(DWORD a, LPDDCOLORKEY b);
	virtual HRESULT WINAPI	SetOverlayPosition(LONG a, LONG b);
	virtual HRESULT WINAPI	SetPalette(LPDIRECTDRAWPALETTE a);
	virtual HRESULT WINAPI	Unlock(LPVOID a);
	virtual HRESULT WINAPI	UpdateOverlay(LPRECT a, LPDIRECTDRAWSURFACE b, LPRECT c, DWORD d, LPDDOVERLAYFX e);
	virtual HRESULT WINAPI	UpdateOverlayDisplay(DWORD a);
	virtual HRESULT WINAPI	UpdateOverlayZOrder(DWORD a, LPDIRECTDRAWSURFACE b);

	IDirectDrawSurface*		GetOriginal();
  
 private:
	IDirectDrawSurface*		mOriginal;
	ULONG					m_nRefCount;
	unsigned int			m_nHierarchyLevel;
	std::string				m_strTabFill;
};

#endif