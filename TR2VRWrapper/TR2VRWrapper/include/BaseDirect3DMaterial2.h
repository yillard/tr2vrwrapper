#ifndef BASEDIRECT3DMATERIAL2_H_INCLUDED
#define BASEDIRECT3DMATERIAL2_H_INCLUDED

#include <d3d.h>
#include <string>


class BaseDirect3DMaterial2 : public IDirect3DMaterial2
{
public:
	BaseDirect3DMaterial2(IDirect3DMaterial2 * aOriginal, unsigned int CreatorHierarchyLevel);
	virtual ~BaseDirect3DMaterial2();

	virtual HRESULT WINAPI	QueryInterface(REFIID riid, LPVOID * ppvObj);
	virtual ULONG WINAPI	AddRef();
	virtual ULONG WINAPI	Release();
	virtual HRESULT WINAPI	SetMaterial(LPD3DMATERIAL a);
	virtual HRESULT WINAPI	GetMaterial(LPD3DMATERIAL a);
	virtual HRESULT WINAPI	GetHandle(LPDIRECT3DDEVICE2 a, LPD3DMATERIALHANDLE b);

private:
	IDirect3DMaterial2*		mOriginal;
	ULONG					m_nRefCount;
	unsigned int			m_nHierarchyLevel;
	std::string				m_strTabFill;
};

#endif