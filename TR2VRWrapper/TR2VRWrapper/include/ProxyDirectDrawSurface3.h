
#include <ddraw.h>
//#include "BaseDirectDrawSurface3.h"


#define MAX_DISTARRAYSIZE 1920*1080

class ProxyDirectDrawSurface3 : public BaseDirectDrawSurface3
{
public:
	ProxyDirectDrawSurface3 (IDirectDrawSurface3* aOriginal, unsigned int CreatorHierarchyLevel);
	virtual ~ProxyDirectDrawSurface3();

	virtual HRESULT WINAPI AddAttachedSurface(LPDIRECTDRAWSURFACE3 a);
	virtual HRESULT WINAPI Flip(LPDIRECTDRAWSURFACE3 a, DWORD b);

private:
	//void DistortionCalc(int SurfaceWidth, int SurfaceHeight);

	bool DistortionCalculated;
	unsigned int DistortedCoord[MAX_DISTARRAYSIZE];
};