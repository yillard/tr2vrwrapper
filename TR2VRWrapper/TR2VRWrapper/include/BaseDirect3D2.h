#ifndef BASEDIRECT3D2_H_INCLUDED
#define BASEDIRECT3D2_H_INCLUDED

#include <d3d.h>
#include <string>


class BaseDirect3D2 : public IDirect3D2
{
public:
	BaseDirect3D2(IDirect3D2 *aOriginal, unsigned int CreatorHierarchyLevel, BaseDirectDraw2* pCreatedBy);
	virtual ~BaseDirect3D2();

	virtual HRESULT WINAPI	QueryInterface(REFIID riid, LPVOID * ppvObj);
	virtual ULONG WINAPI	AddRef();
	virtual ULONG WINAPI	Release();
	virtual HRESULT WINAPI	EnumDevices(LPD3DENUMDEVICESCALLBACK a, LPVOID b);
	virtual HRESULT WINAPI	CreateLight(LPDIRECT3DLIGHT * a, IUnknown * b);
	virtual HRESULT WINAPI	CreateMaterial(LPDIRECT3DMATERIAL2 * a, IUnknown * b);
	virtual HRESULT WINAPI	CreateViewport(LPDIRECT3DVIEWPORT2 * a, IUnknown * b);
	virtual HRESULT WINAPI	FindDevice(LPD3DFINDDEVICESEARCH a, LPD3DFINDDEVICERESULT b);
	virtual HRESULT WINAPI	CreateDevice(REFCLSID a, LPDIRECTDRAWSURFACE b, LPDIRECT3DDEVICE2 * c);

	BaseDirectDraw2*	GetDDInterface();

private:
	IDirect3D2*			mOriginal;
	BaseDirectDraw2*	mCreatedBy;
	ULONG				m_nRefCount;
	unsigned int		m_nHierarchyLevel;
	std::string			m_strTabFill;
};


#endif