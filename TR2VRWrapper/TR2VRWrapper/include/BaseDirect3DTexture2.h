#ifndef BASEDIRECT3DTEXTURE2_H_INCLUDED
#define BASEDIRECT3DTEXTURE2_H_INCLUDED

#include <d3d.h>
#include <string>


class BaseDirect3DTexture2 : public IDirect3DTexture2
{
public:
	BaseDirect3DTexture2(IDirect3DTexture2 * aOriginal, unsigned int CreatorHierarchyLevel);
	virtual ~BaseDirect3DTexture2();

	virtual HRESULT WINAPI	QueryInterface(REFIID riid, LPVOID * ppvObj);
	virtual ULONG WINAPI	AddRef();
	virtual ULONG WINAPI	Release();
	virtual HRESULT WINAPI	GetHandle(LPDIRECT3DDEVICE2 a, LPD3DTEXTUREHANDLE b);
	virtual HRESULT WINAPI	PaletteChanged(DWORD a, DWORD b);
	virtual HRESULT WINAPI	Load(LPDIRECT3DTEXTURE2 a);

	IDirect3DTexture2*		GetOriginal();

private:
	IDirect3DTexture2*		mOriginal;
	ULONG					m_nRefCount;
	unsigned int			m_nHierarchyLevel;
	std::string				m_strTabFill;
};

#endif;